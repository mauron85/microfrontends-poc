# Microfrontends Proof-of-Concept

## Install dependencies

Warning: Don't use just `yarn`! IN WON'T INSTALL ALL PACKAGES.

```
yarn install
```

## Build packages

This is required step, since it will build shared ui-components library

```
yarn build
```

## Start development server
```
yarn start
```

## Start constainer
```
yarn start:container
```

Visit: http://localhost:8000/

## Links

https://carloscuesta.me/blog/javascript-monorepos-lerna-yarn-workspaces/
https://medium.com/viewstools/how-to-use-yarn-workspaces-with-create-react-app-and-create-react-native-app-expo-to-share-common-ea27bc4bad62

