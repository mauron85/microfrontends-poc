/* eslint-disable prettier/prettier */
export const HOME = '/'
export const DPC = '/autopilot'
export const DASHBOARD = '/dashboard'
export const LOGIN = '/login'
export const SCHEDULES = '/schedules'
export const ALERTS = '/notification-centre'
export const CONTACTS = '/contact-center'
export const TRANSPONDER_MAP = '/transponder-map';
export const COMPONENT_TEST_PAGE = '/components-test'
export const BLADE_TEST_PAGE = '/autopilot/blade-test'
