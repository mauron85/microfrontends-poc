const ROLES = {
  AUTOPILOT_CLASSIC: '56dbcf79-da0e-4adb-9705-bdec2c4afb50',
  RADAR_CLASSIC: '0546d04e-d873-4a71-b5c8-983eb2d8183e',
  SYS_ADMIN_ROLE: '5816e978-8df6-4152-ba64-8db785ae33de',
  HELPDESK_LEVEL_1: 'e172880f-df69-4d36-a607-c779053e9405',
  CIO_MID: '51c30f79-7db0-4386-86e1-f86d7ca4efb7',
  ACCOUNT_ADMIN: '5d481cb8-6687-41dc-8406-b8607ccd2428',
  USER_ACCESS: 'f1b323a7-8910-4110-ad25-9a8a7601a10f',
  COJAC_ADMIN: '10bf83e3-e5bf-4667-a48f-32d49ab07a46',
  ORG_UNIT_ADMIN: '9ff38689-8490-4721-98b3-62ee22acb101',
  LICENSE_ADMIN: '1b204e94-cb9b-4d37-bbea-66b03389c019',
  TMS_ACCOUNTS_ADMIN: '01c32efd-a62f-4e11-a5a3-96e2890978ad',
  TMS_ADMIN: 'd527e637-e270-489a-a1bc-9002297aba38',
  TMS_VIEWER: 'efd1863b-5685-4846-9dac-adcf160fa523',
  AUTH_POLICY_ADMIN: '8763ba80-b05b-467e-a550-3a4789d5274b',
  ALL_ROLES: 'anyRole',
};

export default ROLES;

export const DEFAULT_ACCESS_ROLE = {
  roleId: ROLES.USER_ACCESS,
  description:
    'Can login to the walled garden (Portal.Quadro.tech) only. Cannot view data of the current or sub organizations, cannot manage current or sub organizations.',
  displayName: 'User Access',
  isSystemRole: true,
  isVisible: true,
  shortCode: 'useraccess',
};

export const ROLES_IN_ORDER_OF_PRECEDENCE = [
  ROLES.TMS_ADMIN,
  ROLES.TMS_VIEWER,
  ROLES.SYS_ADMIN_ROLE,
  ROLES.ACCOUNT_ADMIN,
  ROLES.CIO_MID,
  ROLES.HELPDESK_LEVEL_1,
  ROLES.AUTOPILOT_CLASSIC,
  ROLES.RADAR_CLASSIC,
  ROLES.USER_ACCESS,
];
