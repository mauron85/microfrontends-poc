import roles from './roles'
import {
  AUTOPILOT_USER_PROFILE,
  AUTOPILOT_USERS,
  AUTOPILOT_MAIL_CONTACTS,
  AUTOPILOT_TENANTS,
  AUTOPILOT_ON_PREMISES_DOMAINS,
  AUTOPILOT_ON_PREMISES_AGENT,
  AUTOPILOT_AUTH_POLICIES,
  AUTOPILOT_LICENSE_POLICIES,
  AUTOPILOT_JOB_ACTIVITY,
  AUTOPILOT_JOB_SCHEDULE,
  AUTOPILOT_AUDIT_LOG,
  AUTOPILOT_LICENSES,
  AUTOPILOT_TEAMS,
  AUTOPILOT_ADMIN_SERVICE_ACCOUNTS,
  AUTOPILOT_MAILBOXES,
  AUTOPILOT_GROUPS,
  AUTOPILOT_CONFIG_POLICIES
} from '../routes/constants/routeConstants';
import { HOME, TRANSPONDER_MAP, COMPONENT_TEST_PAGE, BLADE_TEST_PAGE } from './additionalRoutes'

const {
  SYS_ADMIN_ROLE,
  ALL_ROLES,
  AUTOPILOT_CLASSIC,
  CONFIG_POLICY_ADMIN,
  ACCOUNT_ADMIN,
  AUTH_POLICY_ADMIN,
  LICENSE_ADMIN,
  ORG_UNIT_ADMIN
} = roles

const dpcSidebarConfig = {
  endpoints: ['Autopilot API'],
  displayIfNoAccess: true,
  subscriptions: [
    {
      product: 'DPC',
      feature: 'Core'
    }
  ]
}

const transponderSidebarConfig = {
  endpoints: ['Transponder API'],
  displayIfNoAccess: true,
  subscriptions: [
    {
      product: 'Availability',
      feature: 'Core',
    },
  ],
}

export const sidebarConfig = [
  // {
  //   name: 'Dev',
  //   links: [
  //     {
  //       name: 'Components',
  //       url: COMPONENT_TEST_PAGE,
  //       roles: [ALL_ROLES]
  //     },
  //     {
  //       name: 'Blade Test',
  //       url: BLADE_TEST_PAGE,
  //       roles: [ALL_ROLES]
  //     }
  //   ],
  //   roles: [ALL_ROLES]
  // },
  {
    name: 'Home',
    icon: 'Home',
    url: HOME,
    roles: [ALL_ROLES]
  },
  // {
  //   name: 'User profile',
  //   url: AUTOPILOT_USER_PROFILE,
  //   roles: [ALL_ROLES],
  //   icon: 'Contact',
  //   ...dpcSidebarConfig
  // },
  // {
  //   name: 'Licensing',
  //   links: [
  //     {
  //       name: 'License Management',
  //       url: AUTOPILOT_LICENSES,
  //       icon: 'Medal',
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN, LICENSE_ADMIN],
  //       ...dpcSidebarConfig
  //     }
  //   ],
  //   roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN, LICENSE_ADMIN],
  //   ...dpcSidebarConfig
  // },
  {
    name: 'Manage',
    links: [
      {
        name: 'Users ',
        icon: 'People',
        url: AUTOPILOT_USERS,
        roles: [
          SYS_ADMIN_ROLE,
          AUTOPILOT_CLASSIC,
          ACCOUNT_ADMIN,
          AUTH_POLICY_ADMIN,
          LICENSE_ADMIN,
          ORG_UNIT_ADMIN,
          CONFIG_POLICY_ADMIN
        ],
        ...dpcSidebarConfig
      },
      // {
      //   name: 'Contacts',
      //   icon: 'ContactCard',
      //   url: AUTOPILOT_MAIL_CONTACTS,
      //   roles: [
      //     SYS_ADMIN_ROLE,
      //     AUTOPILOT_CLASSIC,
      //     ACCOUNT_ADMIN,
      //     AUTH_POLICY_ADMIN,
      //     LICENSE_ADMIN,
      //     ORG_UNIT_ADMIN,
      //     CONFIG_POLICY_ADMIN
      //   ],
      //   ...dpcSidebarConfig
      // },
      // {
      //   name: 'Mailboxes',
      //   icon: 'Mail',
      //   url: AUTOPILOT_MAILBOXES,
      //   roles: [
      //     SYS_ADMIN_ROLE,
      //     AUTOPILOT_CLASSIC,
      //     ACCOUNT_ADMIN,
      //     AUTH_POLICY_ADMIN,
      //     LICENSE_ADMIN,
      //     ORG_UNIT_ADMIN,
      //     CONFIG_POLICY_ADMIN
      //   ],
      //   ...dpcSidebarConfig
      // },
      // {
      //   name: 'Groups',
      //   icon: 'Group',
      //   url: AUTOPILOT_GROUPS,
      //   roles: [
      //     SYS_ADMIN_ROLE,
      //     AUTOPILOT_CLASSIC,
      //     ACCOUNT_ADMIN,
      //     AUTH_POLICY_ADMIN,
      //     LICENSE_ADMIN,
      //     ORG_UNIT_ADMIN,
      //     CONFIG_POLICY_ADMIN
      //   ],
      //   ...dpcSidebarConfig
      // },
      // {
      //   name: 'Teams',
      //   icon: 'Teamwork',
      //   url: AUTOPILOT_TEAMS,
      //   roles: [
      //     SYS_ADMIN_ROLE,
      //     AUTOPILOT_CLASSIC,
      //     ACCOUNT_ADMIN,
      //     AUTH_POLICY_ADMIN,
      //     LICENSE_ADMIN,
      //     ORG_UNIT_ADMIN,
      //     CONFIG_POLICY_ADMIN
      //   ],
      //   ...dpcSidebarConfig
      // }
    ],
    roles: [
      SYS_ADMIN_ROLE,
      AUTOPILOT_CLASSIC,
      ACCOUNT_ADMIN,
      AUTH_POLICY_ADMIN,
      LICENSE_ADMIN,
      ORG_UNIT_ADMIN,
      CONFIG_POLICY_ADMIN
    ],
    ...dpcSidebarConfig
  },
  // {
  //   name: 'Manage administration',
  //   links: [
  //     {
  //       name: 'Audit log',
  //       icon: 'FileASPX',
  //       url: AUTOPILOT_AUDIT_LOG,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'Authorization policies',
  //       icon: 'AuthenticatorApp',
  //       url: AUTOPILOT_AUTH_POLICIES,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN, AUTH_POLICY_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'Configuration policies',
  //       icon: 'PlayerSettings',
  //       url: AUTOPILOT_CONFIG_POLICIES,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN, CONFIG_POLICY_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'Jobs',
  //       icon: 'Work',
  //       url: AUTOPILOT_JOB_ACTIVITY,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'Job schedules',
  //       icon: 'WorldClock',
  //       url: AUTOPILOT_JOB_SCHEDULE,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'License policies',
  //       icon: 'ReportHacked',
  //       url: AUTOPILOT_LICENSE_POLICIES,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN, LICENSE_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'On premises domains',
  //       icon: 'LinkedDatabase',
  //       url: AUTOPILOT_ON_PREMISES_DOMAINS,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'On premises agent',
  //       icon: 'Link12',
  //       url: AUTOPILOT_ON_PREMISES_AGENT,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'Service accounts',
  //       icon: 'CRMServices',
  //       url: AUTOPILOT_ADMIN_SERVICE_ACCOUNTS,
  //       roles: [SYS_ADMIN_ROLE, ACCOUNT_ADMIN],
  //       ...dpcSidebarConfig
  //     },
  //     {
  //       name: 'Tenants',
  //       icon: 'DOM',
  //       url: AUTOPILOT_TENANTS,
  //       roles: [
  //         SYS_ADMIN_ROLE,
  //         ACCOUNT_ADMIN,
  //         AUTH_POLICY_ADMIN,
  //         LICENSE_ADMIN,
  //         ORG_UNIT_ADMIN,
  //         CONFIG_POLICY_ADMIN
  //       ],
  //       ...dpcSidebarConfig
  //     }
  //   ],
  //   roles: [
  //     SYS_ADMIN_ROLE,
  //     ACCOUNT_ADMIN,
  //     AUTH_POLICY_ADMIN,
  //     LICENSE_ADMIN,
  //     ORG_UNIT_ADMIN,
  //     CONFIG_POLICY_ADMIN
  //   ],
  //   ...dpcSidebarConfig
  // },
  {
    name: 'Monitoring',
    links: [
      {
        name: 'Dashboard',
        url: TRANSPONDER_MAP,
        icon: 'World',
        roles: [ALL_ROLES],
        subscriptions: [
          {
            product: 'Availability',
            feature: 'Core',
          },
        ],
      },
      // {
      //   name: 'Service Status',
      //   url: RADAR_SERVICE_STATUS_PAGE,
      //   roles: [ALL_ROLES],
      //   subscriptions: [
      //     {
      //       product: 'Reporting',
      //       feature: 'Core',
      //     },
      //   ],
      //   endpoints: ['Radar'],
      //   displayIfNoAccess: true,
      // },
    ],
    roles: [ALL_ROLES],
    subscriptions: [
      {
        product: 'Availability',
        feature: 'Core',
      },
    ],
    ...transponderSidebarConfig,
  },
]

export default sidebarConfig;
