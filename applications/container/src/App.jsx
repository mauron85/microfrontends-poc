import React, { useEffect, useState } from 'react';
import { I18nextProvider } from 'react-i18next';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import {
  StoreProvider,
  StateContext,
  PrivateRoute as PrivRoute,
  useStore,
} from '@quadrotech/core-components';
import {
  SideBar,
  NotFound,
  LoginModal,
  LoadingSpinner,
  ThemeProvider,
  theme,
} from '@quadrotech/ui-components';
import { Header, Content, Body, Dashboard, MicroFrontend } from './components';
import PrivateRoute from './components/PrivateRoute';
import SideBar2 from './SideBar';
import sidebarConfig from './config/sideBar';
import delay from './utils/delay';
import i18n from './i18n';

const {
  REACT_APP_API_HOST: apiHost,
  REACT_APP_AUTOPILOT_HOST: autopilotHost,
  REACT_APP_RADAR_HOST: radarHost,
  REACT_APP_TRANSPONDER_HOST: transponderHost,
} = process.env;

const initialState = {
  user: {
    isAuthenticated: false,
  },
};

const reducers = {};

const Autopilot = ({ history }) => (
  <MicroFrontend history={history} host={autopilotHost} name="Autopilot" />
);

const Radar = ({ history }) => <MicroFrontend history={history} host={radarHost} name="Radar" />;

const Transponder = ({ history }) => (
  <MicroFrontend history={history} host={transponderHost} name="Transponder" />
);

const App = () => {
  // const [{ user }, dispatch] = useStore();
  const [user, setUser] = useState({ isAuthenticated: false, isFetching: true });

  useEffect(() => {
    let mounted = true;

    const loadData = async () => {
      const host = process.env.REACT_APP_API_HOST;
      const result = await fetch(`${host}/tms/user/info`, { credentials: 'include' });
      if (mounted && result.status === 200) {
        const user = await result.json();
        await delay(1000); // simulate slow connection
        setUser({ ...user, isAuthenticated: true, isFetching: false });
      } else {
        setUser({ ...user, isFetching: false });
      }
    };
    loadData();

    return () => {
      // When cleanup is called, toggle the mounted variable to false
      mounted = false;
    };
  }, []);

  return (
    <StoreProvider initialState={initialState} reducer={reducers}>
      <ThemeProvider theme={theme}>
        <I18nextProvider i18n={i18n}>
          <Router>
            <Header />
            {/* <StateContext.Consumer>
              {([state]) => (
                <LoginModal
                  launchSigninRedirect={() => {
                    const currentLocationUri = encodeURIComponent(window.location.origin);
                    window.location.href = `${apiHost}/login?redirect_uri=${currentLocationUri}`;
                  }}
                  isOpen={!state.user.isAuthenticated}
                />
              )}
            </StateContext.Consumer> */}
            <LoginModal
              launchSigninRedirect={() => {
                const currentLocationUri = encodeURIComponent(window.location.origin);
                window.location.href = `${apiHost}/login?redirect_uri=${currentLocationUri}`;
              }}
              isOpen={!user.isFetching && !user.isAuthenticated}
            />
            <Body>
              {/* <SideBar config={sidebarConfig} /> */}
              <SideBar2 />
              <Content>
                {(!user.isFetching && (
                  <Switch>
                    <Redirect exact from="/" to="/dashboard" />
                    <PrivateRoute path="/autopilot" user={user} component={Autopilot} />
                    <PrivateRoute path="/radar" user={user} component={Radar} />
                    <PrivateRoute path="/transponder*" user={user} component={Transponder} />
                    <PrivateRoute exact path="/dashboard" user={user} component={Dashboard} />
                    <Route exact path="/signin" user={user} component={NotFound} />
                    <Route component={NotFound} />
                  </Switch>
                )) || <LoadingSpinner />}
              </Content>
            </Body>
          </Router>
        </I18nextProvider>
      </ThemeProvider>
    </StoreProvider>
  );
};

export default App;
