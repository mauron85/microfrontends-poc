import { Route, useHistory } from 'react-router-dom';

const PrivateRoute = ({ children, component: Component, user, ...rest }) => {
  const history = useHistory();

  // useEffect(() => {
  if (!user.isAuthenticated) {
    history.push('/signin');
    return null;
  }
  // }, [user]);

  return <Route {...rest} render={props => <Component {...props} user={user} />} />;
};

export default PrivateRoute;
