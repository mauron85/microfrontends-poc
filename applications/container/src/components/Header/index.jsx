import styled from 'styled-components';
import { theme } from '@quadrotech/ui-components';

const Header = styled.header`
  height: 50px;
  background-color: ${theme.defaultTheme.themeHeaderBgColor};
`;

const Title = styled.h1`
  margin: 0 0 0 5px;
  line-height: 1.3;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: #71afe5;
  font-size: 17px;
  font-weight: 600;
`;

export default () => (
  <Header>
    <Title>Micro frontends demo</Title>
  </Header>
);
