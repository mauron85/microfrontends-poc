import styled from 'styled-components';

const Content = styled.main`
  width: calc(100vw - 250px);
  padding: 10px;
  margin-bottom: 20px;
  background-color: #f4f4f4;
`;

export default Content;
