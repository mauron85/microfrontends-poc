import Header from './Header';
import Content from './Content';
import Body from './Body';
import Dashboard from './Dashboard';
import MicroFrontend from './MicroFrontend';

export {
  Header,
  Content,
  Body,
  Dashboard,
  MicroFrontend
}
