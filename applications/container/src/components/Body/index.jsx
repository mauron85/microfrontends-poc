import styled from 'styled-components';
import { theme } from '@quadrotech/ui-components';

const Body = styled.section`
  display: flex;
  margin: 0 auto;
  height: 100%;
  width: 100%;
  background-color: ${theme.defaultTheme.themeHeaderBgColor};
`;

export default Body;
