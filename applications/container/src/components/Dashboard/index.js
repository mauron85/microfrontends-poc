import React from 'react';
import styled from 'styled-components';
import { ResponsiveContainer, PieChart, Pie, Sector, Cell, Text } from 'recharts';
import { Content, theme } from '@quadrotech/ui-components';

const Widget = styled.div`
  position: relative;
  max-width: 300px;
  padding: 10px 15px;
  cursor: auto;
  opacity: 1;
  background-color: #ffffff;
`;

const WidgetTitle = styled.div`
  font-size: 1.25rem;
  font-weight: 600;
  text-align: left;
`;

const data = [
  {
    label: 'With license',
    amount: 26,
  },
  {
    label: 'Without license',
    amount: 40,
  },
];

const graphColours = [
  'red',
  'yellow',
  'orange',
  'green',
  'blueLight',
  'greenDark',
  'blueMid',
  'orangeLighter',
  'tealLight',
  'tealDark',
  'magentaLight',
  'purpleDark',
  'yellowLight',
  'magentaDark',
  'teal',
  'magenta',
  'greenLight',
  'brownLight',
  'redDark',
  'purpleLight',
  'brownDark',
  'blue',
  'blueDark',
  'orangeLight',
  'purple',
];

const colorRange = (number = 15, priorityColors = []) => {
  if (priorityColors.length >= number) {
    return [...priorityColors];
  }

  // Omit colours that were already supplied
  const useColors = graphColours.filter(color => !priorityColors.includes(color));
  const generateNumber = number - priorityColors.length;

  const generatedRange = Array(generateNumber)
    .fill()
    .map((value, index) => {
      const colourIndex = index <= useColors.length - 1 ? index : index % useColors.length;
      return useColors[colourIndex];
    });

  return [...priorityColors, ...generatedRange];
};

const END_OFFSET = 16;
const START_OFFSET = 10;
const MIDDLE_OFFSET = 20;
const LabelLine = ({ midAngle, cx, outerRadius, cy, fill }) => {
  const RADIAN = Math.PI / 180;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const startX = cx + (outerRadius + START_OFFSET) * cos;
  const startY = cy + (outerRadius + START_OFFSET) * sin;
  const middleX = cx + (outerRadius + MIDDLE_OFFSET) * cos;
  const middleY = cy + (outerRadius + MIDDLE_OFFSET) * sin;
  const endX = middleX + (cos >= 0 ? 1 : -1) * END_OFFSET;
  const endY = middleY;

  return (
    <path
      d={`M${startX},${startY}L${middleX},${middleY}L${endX},${endY}`}
      stroke={fill}
      fill="none"
    />
  );
};

const OUTER_RING_OFFSET_BEGIN = 6;
const OUTER_RING_OFFSET_END = 10;
const LABEL_ENDCAP_RADIUS = 2;
const LABEL_POSITION_OFFSET = 12;
const NAME_OFFSET = 8;
const PRIMARY_LABEL_OFFSET = 4;
const SECONDARY_LABEL_OFFSET = 18;
const TEXT_WIDTH = 100;
const CustomActiveShape = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  startAngle,
  endAngle,
  fill,
  payload,
  percent,
  value,
}) => {
  const labelLineProps = {
    cx,
    cy,
    midAngle,
    outerRadius,
    fill,
  };

  const RADIAN = Math.PI / 180;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const middleX = cx + (outerRadius + MIDDLE_OFFSET) * cos;
  const middleY = cy + (outerRadius + MIDDLE_OFFSET) * sin;
  const shouldBeLeftAligned = cos >= 0;
  const endX = middleX + (shouldBeLeftAligned ? 1 : -1) * END_OFFSET;
  const endY = middleY;
  const textAnchor = shouldBeLeftAligned ? 'start' : 'end';

  const { label } = payload;

  return (
    <g>
      <text x={cx} y={cy} dy={NAME_OFFSET} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>

      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />

      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + OUTER_RING_OFFSET_BEGIN}
        outerRadius={outerRadius + OUTER_RING_OFFSET_END}
        fill={fill}
      />

      <LabelLine {...labelLineProps} />

      <circle cx={endX} cy={endY} r={LABEL_ENDCAP_RADIUS} fill={fill} stroke="none" />

      <Text
        x={endX + (shouldBeLeftAligned ? 1 : -1) * LABEL_POSITION_OFFSET}
        y={endY}
        dy={PRIMARY_LABEL_OFFSET}
        textAnchor={textAnchor}
        fill="#333"
        width={TEXT_WIDTH}
      >
        {label}
      </Text>

      <Text
        x={endX + (shouldBeLeftAligned ? 1 : -1) * LABEL_POSITION_OFFSET}
        y={endY}
        dy={SECONDARY_LABEL_OFFSET}
        textAnchor={textAnchor}
        fill="#999"
        fontSize="10px"
        width={TEXT_WIDTH}
      >
        {`${value} (${(percent * 100).toFixed(2)}%)`}
      </Text>
    </g>
  );
};

const renderActiveShape = props => {
  const RADIAN = Math.PI / 180;
  const {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    percent,
    value,
  } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        textAnchor={textAnchor}
        fill="#333"
      >{`PV ${value}`}</text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(Rate ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

class TwoLevelPieChart extends React.Component {
  constructor() {
    super();
    this.state = {
      activeIndex: 0,
    };
  }

  onPieEnter = (data, index) => {
    this.setState({
      activeIndex: index,
    });
  };

  changeCellColor(fill) {
    switch (fill) {
      case '#00bcf2':
        return '#6CDEFF';
      case '#acd336':
        return '#D9EBA3';
    }
    return '';
  }

  render() {
    const { activeIndex: activeSectionIndex } = this.state;
    const { label, data, theme } = this.props;
    const labelField = 'label';
    const dataSample = data[0];
    const dataKeys = dataSample ? Object.keys(dataSample).filter(key => key !== labelField) : [];
    const fillColours = ['green', 'blueLight'];

    return (
      <PieChart width={400} height={300}>
        {dataKeys.map(dataKey => (
          <Pie
            key={`pie${dataKey}`}
            activeIndex={activeSectionIndex}
            activeShape={CustomActiveShape}
            data={data}
            dataKey={dataKey}
            nameKey={labelField}
            cx={150}
            cy={150}
            innerRadius={60}
            outerRadius={80}
            isAnimationActive={false}
            fill="#8884d8"
            onMouseEnter={this.onPieEnter}
          >
            {/* {label && <Label value={label} position="center" />} */}
            {data.map((entry, index) => {
              const fill = theme[fillColours[index]];
              const changeColor = index === activeSectionIndex ? this.changeCellColor(fill) : fill;
              return (
                <Cell
                  key={`cell${entry.label}`}
                  fill={changeColor}
                  onMouseLeave={this.handleCellLeave}
                  onClick={this.handleCellClick}
                />
              );
            })}
          </Pie>
        ))}
      </PieChart>
    );
  }
}

const Dashboard = () => {
  return (
    <div>
      <Content.Header>
        <Content.Title>Dashboard</Content.Title>
      </Content.Header>
      <Widget>
        <WidgetTitle>Licenses</WidgetTitle>
        <ResponsiveContainer>
          <TwoLevelPieChart data={data} theme={theme.defaultTheme} />
        </ResponsiveContainer>
      </Widget>
    </div>
  );
};

export default Dashboard;
