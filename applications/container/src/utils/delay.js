const delay = (millis) => new Promise(resolve => setTimeout(resolve, millis));

export default delay;
