import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const Menu = styled.ul`
  color: white;
  margin: 10px;
  padding: 0;
  width: 200px;
  font-size: 1.2em;
`;

const MenuItem = styled.li`
  color: white;
  line-height: 2em;
`;

const Sidebar = () => (
  <nav>
    <Menu>
      <MenuItem>
        <NavLink className="sidebar-link" to="/">
          Dashboard
        </NavLink>
      </MenuItem>
      <MenuItem>
        <NavLink className="sidebar-link" to="/autopilot/users">
          Users
        </NavLink>
      </MenuItem>
      <MenuItem>
        <NavLink className="sidebar-link" to="/radar">
          Radar
        </NavLink>
      </MenuItem>
      <MenuItem>
        <NavLink className="sidebar-link" to="/transponder">
          Transponder
        </NavLink>
      </MenuItem>
    </Menu>
  </nav>
);

export default Sidebar;
