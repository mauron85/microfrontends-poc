const { override, addBabelPlugins, disableEsLint } = require('customize-cra');
const rewireYarnWorkspaces = require('react-app-rewire-yarn-workspaces');
const rewireMicroFrontends = require('react-app-rewire-micro-frontends');

// https://github.com/pdme/compound
const compound = (...fns) => (...args) =>
  fns.reduceRight((result, fn) => fn(...[result, ...args.slice(1)]), args[0]);

const rewireOverrides = (config, env) => {
  return compound(rewireYarnWorkspaces, rewireMicroFrontends)(config, env);
};

module.exports = override(
  rewireOverrides,
  // TODO: Investigate problems with including custom eslint config
  // using useEslintRc('.eslintrc'),
  disableEsLint(),
  ...addBabelPlugins('@babel/plugin-proposal-export-namespace-from'),
);
