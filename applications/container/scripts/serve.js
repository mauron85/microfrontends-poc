const path = require('path');
const express = require('express');
const cors = require('cors');
const http = require('http');

const { PORT = 8000 } = process.env;

const publicDir = path.resolve(__dirname, '..', 'build');
const app = express();
app.use(cors())
app.use(express.static(publicDir, { 'index': ['index.html', 'index.htm'] }));
app.get('/*', (req, res) => {
  res.sendFile(path.join(publicDir, 'index.html'));
});

const server = http.createServer(app);
server.listen(PORT, () => {
  console.log(`Running at http://localhost:${PORT}`);
});
