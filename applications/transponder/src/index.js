import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

window.renderTransponder = (containerId, history) => {
  ReactDOM.render(
    <App history={history} />,
    document.getElementById(containerId),
  );
};

window.unmountTransponder = containerId => {
  ReactDOM.unmountComponentAtNode(document.getElementById(containerId));
};
