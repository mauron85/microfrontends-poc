const rewireMicroFrontends = require('react-app-rewire-micro-frontends');

module.exports = function override(config, env) {
  return rewireMicroFrontends(config, env);
}
