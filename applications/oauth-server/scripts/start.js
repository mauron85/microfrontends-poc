/**
 * Module dependencies.
 */

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const session = require('express-session');
const OAuthServer = require('express-oauth-server');
const querystring = require('qs');
const InMemoryModel = require('./model');

const { PORT = 4001 } = process.env;

// Create an Express application.
const app = express();
const model = new InMemoryModel();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(cookieParser());
app.use(
  session({
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 30000, secure: false },
    secret: 'ogN92SiSSVpUsEAL'
  })
);

// Add OAuth server.
app.oauth = new OAuthServer({
  model,
  grants: ['authorization_code', 'refresh_token'],
  accessTokenLifetime: 60 * 60 * 24, // 24 hours, or 1 day
  allowEmptyState: true,
  useErrorHandler: true,
  requireClientAuthentication: {
    authorization_code: false,
    client_credentials: false
  }
});

const authorizeHandler = app.oauth.authorize({
  authenticateHandler: {
    handle: req => {
      return req.body;
    }
  }
});

// Add body parser.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Post token.
app.post('/oauth/test', (req, res) => {
  res.send(req.body);
});
app.post('/oauth/token', app.oauth.token());

// Get authorization.
app.get(
  '/oauth/authorize',
  (req, res, next) => {
    const { response_type, client_id, redirect_uri, login_hint } = req.query;
    const { user } = req.session;

    const client = model.getClient(client_id);

    const query = querystring.stringify({
      response_type,
      client_id,
      redirect: req.path,
      redirect_uri,
      login_hint
    });

    if (!user) {
      return res.redirect(`/login?${query}`);
    }

    const auth = model.getUserAuthorization(user.id);
    if (!auth) {
      return res.render('authorize.ejs', {
        actionUrl: `/oauth/authorize?${query}`,
        client_id,
        redirect_uri,
        app_name: client ? client.appName : client_id,
        user_id: user.id,
        error: null
      });
    }

    req.body = user;
    return next();
  },
  authorizeHandler
);

// Post authorization.
app.post(
  '/oauth/authorize',
  (req, res, next) => {
    const { client_id, redirect_uri } = req.query;
    const { user } = req.session;

    // Redirect anonymous users to login page.
    if (!user) {
      const query = querystring.stringify({
        redirect: req.path,
        client_id,
        redirect_uri
      });
      return res.redirect(`/login?${query}`);
    }

    req.body = user;
    return next();
  },
  authorizeHandler
);

app.get('/login', (req, res) => {
  const {
    response_type,
    client_id,
    redirect,
    redirect_uri,
    login_hint
  } = req.query;

  const query = querystring.stringify({
    response_type,
    redirect,
    client_id,
    redirect_uri
  });

  return res.render('login.ejs', {
    actionUrl: `/login?${query}`,
    client_id,
    redirect_uri,
    username: login_hint,
    error: null
  });
});

// Post login.
app.post('/login', (req, res) => {
  const { response_type, redirect, client_id, redirect_uri } = req.query;
  const { login, passwd } = req.body;

  const user = model.getUser(login, passwd);
  if (!user) {
    const query = querystring.stringify({
      redirect,
      client_id,
      redirect_uri
    });

    return res.render('login.ejs', {
      actionUrl: `/login?${query}`,
      client_id,
      redirect_uri,
      username: login,
      error: 'Invalid credentials'
    });
  }

  req.session.user = user;

  // Successful logins should send the user back to /oauth/authorize.
  const path = redirect || '/oauth/authorize';
  const query = querystring.stringify({
    response_type,
    client_id,
    redirect_uri
  });

  return res.redirect(`${path}?${query}`);
});

app.get('/user/info', app.oauth.authenticate(), (req, res) => {
  // Will require a valid access_token.
  const { password, ...user } = res.locals.oauth.token.user;
  res.json(user);
});


app.get('/public', (req, res) => {
  // Does not require an access_token.
  res.send('Public area');
});

// Start listening for requests.
app.listen(PORT, () => {
  console.log(`OAuth Server running at http://localhost:${PORT}`);
});
