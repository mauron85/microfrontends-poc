/**
 * Constructor.
 */

function InMemoryCache() {
  this.clients = [
    {
      id: 'microfrontends-poc',
      appName: 'Microfrontends DEMO',
      clientSecret: 'nightworld',
      redirectUris: [
        'http://localhost:4000/auth/callback',
        'http://localhost:4000/auth/callback?redirect_uri=http://localhost:8000',
        'http://localhost:8000'
      ],
      grants: ['authorization_code', 'refresh_token']
    }
  ];
  this.tokens = [];
  this.users = [
    {
      id: '123',
      username: 'microfrontends@quadrotech.com',
      password: 'demo123$!'
    }
  ];
  this.authorizationCodes = [];
}

/**
 * Dump the cache.
 */

InMemoryCache.prototype.dump = function() {
  console.log('clients', this.clients);
  console.log('tokens', this.tokens);
  console.log('users', this.users);
  console.log('codes', this.authorizationCodes);
};

/*
 * Get access token.
 */

InMemoryCache.prototype.getAccessToken = function(bearerToken) {
  var tokens = this.tokens.filter(function(token) {
    return token.accessToken === bearerToken;
  });

  const token = tokens[0];
  if (token) {
    return Object.assign({}, token, {
      client: this.getClient(token.clientId),
      user: this.getUserById(token.userId)
    });
  }

  return false;
};

/**
 * Get refresh token.
 */

InMemoryCache.prototype.getRefreshToken = function(bearerToken) {
  var tokens = this.tokens.filter(function(token) {
    return token.refreshToken === bearerToken;
  });

  return tokens.length ? tokens[0] : false;
};

/**
 * Get client.
 */

InMemoryCache.prototype.getClient = function(clientId, clientSecret) {
  var clients = this.clients.filter(function(client) {
    return client.id === clientId; // && client.clientSecret === clientSecret;
  });

  return clients.length ? clients[0] : false;
};

/**
 * Save token.
 */

InMemoryCache.prototype.saveToken = function(token, client, user) {
  this.tokens.push({
    accessToken: token.accessToken,
    accessTokenExpiresAt: token.accessTokenExpiresAt,
    clientId: client.id,
    refreshToken: token.refreshToken,
    refreshTokenExpiresAt: token.refreshTokenExpiresAt,
    userId: user.id
  });

  return { ...token, client, user };
};

InMemoryCache.prototype.saveAuthorizationCode = function(code, client, user) {
  // imaginary DB queries
  var authCode = {
    authorization_code: code.authorizationCode,
    expires_at: code.expiresAt,
    redirect_uri: code.redirectUri,
    scope: code.scope,
    client_id: client.id,
    user_id: user.id
  };

  this.authorizationCodes.push(authCode);
  return {
    authorizationCode: authCode.authorization_code,
    expiresAt: authCode.expires_at,
    redirectUri: authCode.redirect_uri,
    scope: authCode.scope,
    client: { id: authCode.client_id },
    user: { id: authCode.user_id }
  };
};

InMemoryCache.prototype.getAuthorizationCode = function(authorizationCode) {
  var authorizationCodes = this.authorizationCodes.filter(function(auth) {
    return auth.authorization_code === authorizationCode;
  });

  const code = authorizationCodes[0];
  if (code) {
    return {
      authorizationCode: code.authorization_code,
      expiresAt: code.expires_at,
      redirectUri: code.redirect_uri,
      scope: code.scope,
      client: this.getClient(code.client_id),
      user: this.getUserById(code.user_id)
    };
  }

  return false;
};

InMemoryCache.prototype.revokeAuthorizationCode = function(authorization) {
  var authorizationCodes = this.authorizationCodes.filter(function(auth) {
    return auth.authorization_code !== authorization.authorizationCode;
  });

  if (authorizationCodes.length < this.authorizationCodes.length) {
    this.authorizationCodes = authorizationCodes;
    return authorization;
  }

  return false;
};

/*
 * Get user.
 */

InMemoryCache.prototype.getUser = function(username, password) {
  var users = this.users.filter(function(user) {
    return user.username === username && user.password === password;
  });

  return users.length ? users[0] : false;
};

InMemoryCache.prototype.getUserById = function(userId) {
  var users = this.users.filter(function(user) {
    return user.id === userId;
  });

  return users.length ? users[0] : false;
};

InMemoryCache.prototype.getUserAuthorization = function(userId) {
  var authorizationCodes = this.authorizationCodes.filter(function(auth) {
    return auth.user_id === userId;
  });

  return authorizationCodes.length ? authorizationCodes[0] : false;
};

/**
 * Export constructor.
 */

module.exports = InMemoryCache;
