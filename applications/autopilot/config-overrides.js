const { override, addBabelPlugins, disableEsLint } = require('customize-cra');
const rewireMicroFrontends = require('react-app-rewire-micro-frontends');

const rewireOverrides = (config, env) => {
  return rewireMicroFrontends(config, env);
};

module.exports = override(
  rewireOverrides,
  // TODO: Investigate problems with including custom eslint config
  // using useEslintRc('.eslintrc'),
  disableEsLint(),
  ...addBabelPlugins('@babel/plugin-proposal-export-namespace-from'),
);
