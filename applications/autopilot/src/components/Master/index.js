import React from 'react';
import { Content } from '@quadrotech/ui-components';

const Master = () => {
  return (
    <Content.Header>
      <Content.Title>Autopilot</Content.Title>
    </Content.Header>
  );
};

export default Master;
