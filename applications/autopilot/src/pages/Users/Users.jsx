import React, { useState, useEffect } from 'react';
import { Content } from '@quadrotech/ui-components';
import UsersTable from './UsersTable';

const tableProps = {
  tableKey: 'Users',
  params: { organizationalUnitId: null },
  mappers: {},
  filterable: true,
  columns: [
    {
      Header: 'Name',
      accessor: 'name',
      filterable: true,
      width: 200,
      hideTooltip: true,
      show: true
    },
    {
      Header: 'User principal name',
      accessor: 'upn',
      filterable: true,
      width: 200,
      show: true
    },
    { Header: 'Active status', accessor: 'accountEnabled', show: true },
    { Header: 'Sync status', accessor: 'syncType', show: true },
    { Header: 'Country', accessor: 'country', filterable: true, show: true },
    { Header: 'Usage location', accessor: 'usageLocation', show: true },
    {
      Header: 'Department',
      accessor: 'department',
      filterable: true,
      show: true
    },
    { Header: 'Manager', accessor: 'manager', show: true },
    { Header: 'Organizational unit', accessor: 'ouName', show: true },
    {
      Header: 'Organizational unit path',
      accessor: 'ouPath',
      filterable: false,
      show: true
    },
    { Header: 'Tenant', accessor: 'tenant', filterable: true, show: true },
    {
      Header: 'Evaluate OU rules',
      accessor: 'evaluateOURules',
      width: 140,
      show: true
    },
    {
      Header: 'OU rules conflict',
      accessor: 'ouRulesConflict',
      width: 130,
      show: true
    }
  ],
  hiddenColumns: {},
  disabledRows: [],
  reduxTableActions: {},
  modifyFiltersBeforeChange: null,
  manual: true,
  loading: false,
  pages: 2,
  defaultPageSize: 25,
  sorted: [],
  filtered: [],
  resized: []
};

const Users = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    let mounted = true;

    const loadData = async () => {
      const host = process.env.REACT_APP_API_HOST;
      const result = await fetch(`${host}/users?count=10`);
      if (mounted) {
        const users = await result.json();
        setData(users);
      }
    };
    loadData();

    return () => {
      // When cleanup is called, toggle the mounted variable to false
      mounted = false;
    };
  }, data);

  return (
    <>
      <Content.Header>
        <Content.Title>Users</Content.Title>
      </Content.Header>
      <Content.Body>
        <UsersTable {...tableProps} data={data} />
      </Content.Body>
    </>
  );
};

export default Users;
