import React, { PureComponent } from 'react';
import ReactTable from 'react-table';
import StyledTableWrapper from '../../components/Table/StyledTableWrapper';

export const getTableKey = ({ ouId }) => (ouId ? 'Tenants.Users' : 'Users');
export const mappers = {
  formatRequestBody: (data, params) => ({ document: data, ...params })
};

const TbodyComponent = props => {
  return (
    <TableBodyScrollable>
      <div className="rt-tbody">{props.children}</div>
    </TableBodyScrollable>
  );
};

class UsersTable extends PureComponent {
  render() {
    const { ouId, appliedFilters, data, ...otherProps } = this.props;
    const page = 1;
    const key = getTableKey(this.props);
    return (
      <StyledTableWrapper>
        <ReactTable
          ref={reactTable => {
            this.reactTable = reactTable;
          }}
          pageSizeOptions={[10]}
          data={data}
          columns={otherProps.columns}
          page={page}
          pageSize={10}
        />
      </StyledTableWrapper>
    );
  }
}

UsersTable.defaultProps = {
  ouId: null
};

export default UsersTable;
