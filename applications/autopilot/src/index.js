import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

window.renderAutopilot = (containerId, history) => {
  ReactDOM.render(
    <App history={history} />,
    document.getElementById(containerId),
  );
};

window.unmountAutopilot = containerId => {
  ReactDOM.unmountComponentAtNode(document.getElementById(containerId));
};
