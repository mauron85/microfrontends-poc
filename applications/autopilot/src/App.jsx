import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import AutopilotMaster from './components/Master';
import Users from './pages/Users';
import Loading from './components/Loading';

const defaultHistory = createBrowserHistory();

const App = ({ history }) => {
  return (
    <Router history={history || defaultHistory}>
      <Switch>
        <Route exact path="/autopilot/users" component={Users} />
        <Route component={AutopilotMaster} />
      </Switch>
    </Router>
  );
};

export default App;
