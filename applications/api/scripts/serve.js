const path = require('path');
const request = require('request-promise-native');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const session = require('express-session');
const http = require('http');
const cors = require('cors');
const querystring = require('qs');
const makeUsers = require('../mocks/users');

const {
  PORT = 4000,
  OAUTH_SERVER_URL = 'http://localhost:4001',
  OAUTH_SERVER_CLIENT_ID = 'microfrontends-poc',
  OAUTH_SERVER_CLIENT_SECRET = 'nightworld'
} = process.env;

const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(
  session({
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 30000, secure: false },
    secret: 'rio7ELz6KJB4yBsd'
  })
);
app.use(cors({ origin: true, credentials: true }));
app.use(express.static('public'));

app.get('/ping', (req, res) => {
  res.send("I'm alive");
});

// Get login.
app.get('/login', function(req, res) {
  const { client_id, redirect_uri } = req.query;
  const query = querystring.stringify({
    client_id,
    redirect_uri
  });

  return res.render('login.ejs', {
    actionUrl: `/login?${query}`,
    client_id: client_id,
    redirect_uri: redirect_uri
  });
});

// Post login.
app.post('/login', function(req, res) {
  const { redirect_uri } = req.query;
  const { Username } = req.body;

  const query = querystring.stringify({
    response_type: 'code',
    client_id: OAUTH_SERVER_CLIENT_ID,
    client_secret: OAUTH_SERVER_CLIENT_SECRET,
    redirect_uri: `${req.protocol}://${req.get(
      'host'
    )}/auth/callback?redirect_uri=${redirect_uri}`,
    login_hint: Username
  });

  res.redirect(`${OAUTH_SERVER_URL}/oauth/authorize?${query}`);
});

app.get('/auth/callback', async (req, res, next) => {
  const { redirect_uri, code } = req.query;

  try {
    const token = await request
      .post(`${OAUTH_SERVER_URL}/oauth/token`, { json: true })
      .form({
        code,
        redirect_uri: `${req.protocol}://${req.get(
          'host'
        )}/auth/callback?redirect_uri=${redirect_uri}`,
        client_id: OAUTH_SERVER_CLIENT_ID,
        client_secret: OAUTH_SERVER_CLIENT_SECRET,
        grant_type: 'authorization_code'
      });

    req.session.token = {
      access_token: token.access_token,
      token_type: token.token_type,
      expires_in: token.expires_in,
      expire_date: new Date(Date.now() + token.expires_in * 1000),
      refresh_token: token.refresh_token
    };

    res.redirect(redirect_uri);
  } catch (error) {
    next(error);
  }
});

app.use('/tms', require('./tms'));

app.get('/users', (req, res) => {
  res.send(makeUsers(req.query.count || 20));
});

app.get('/users', (req, res) => {
  res.send(makeUsers(req.query.count || 20));
});

const server = http.createServer(app);
server.listen(PORT, () => {
  console.log(`API Server running at http://localhost:${PORT}`);
});
