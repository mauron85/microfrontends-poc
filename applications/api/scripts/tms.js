const express = require('express');
const request = require('request-promise-native');
const router = express.Router();

const { OAUTH_SERVER_URL = 'http://localhost:4001' } = process.env;

router.get('/user/info', async (req, res, next) => {
  const now = new Date();
  const { token } = req.session;

  if (!token || token.expire_date > now) {
    return res.sendStatus(401);
  }

  try {
    const userInfo = await request.get(`${OAUTH_SERVER_URL}/user/info`, {
      headers: {
        Authorization: `Bearer ${token.access_token}`
      }
    });
    res.json(userInfo);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
