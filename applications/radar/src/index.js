import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

window.renderRadar = (containerId, history) => {
  ReactDOM.render(
    <App history={history} />,
    document.getElementById(containerId),
  );
};

window.unmountRadar = containerId => {
  ReactDOM.unmountComponentAtNode(document.getElementById(containerId));
};
