import _classCallCheck from '@babel/runtime/helpers/classCallCheck';
import _createClass from '@babel/runtime/helpers/createClass';
import _possibleConstructorReturn from '@babel/runtime/helpers/possibleConstructorReturn';
import _getPrototypeOf from '@babel/runtime/helpers/getPrototypeOf';
import _inherits from '@babel/runtime/helpers/inherits';
import React$1, { Component, Fragment } from 'react';
import _assertThisInitialized from '@babel/runtime/helpers/assertThisInitialized';
import _defineProperty from '@babel/runtime/helpers/defineProperty';
import _taggedTemplateLiteral from '@babel/runtime/helpers/taggedTemplateLiteral';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import styled, { css, withTheme, ThemeProvider } from 'styled-components';
import '@babel/runtime/helpers/toConsumableArray';
import { Nav } from 'office-ui-fabric-react/lib/Nav';
import { withNamespaces } from 'react-i18next';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';
import { loadTheme } from 'office-ui-fabric-react/lib/Styling';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';
import { Modal } from 'office-ui-fabric-react/lib/Modal';
import { MessageBar } from 'office-ui-fabric-react/lib/MessageBar';

css(["html{box-sizing:border-box;overflow-y:scroll;-webkit-text-size-adjust:100%;}*,:after,:before{background-repeat:no-repeat;box-sizing:inherit;}:after,:before{text-decoration:inherit;vertical-align:inherit;}:focus{", ";}::-moz-focus-inner{border:0;}*{padding:0;margin:0;}audio:not([controls]){display:none;height:0;}hr{overflow:visible;}article,aside,details,figcaption,figure,footer,header,main,menu,nav,section,summary{display:block;}summary{display:list-item;}small{font-size:80%;}[hidden],template{display:none;}abbr[title]{border-bottom:1px dotted;text-decoration:none;}a{background-color:transparent;-webkit-text-decoration-skip:objects;}a:active,a:hover{outline-width:0;}code,kbd,pre,samp{font-family:monospace,monospace;}b,strong{font-weight:bolder;}dfn{font-style:italic;}mark{background-color:#ff0;color:#000;}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline;}sub{bottom:-0.25em;}sup{top:-0.5em;}input{border-radius:0;}[role='button'],[type='button'],[type='reset'],[type='submit'],button{cursor:pointer;}[disabled]{cursor:default;}[type='search']{-webkit-appearance:textfield;}[type='search']::-webkit-search-cancel-button,[type='search']::-webkit-search-decoration{-webkit-appearance:none;}textarea{overflow:auto;resize:vertical;}button,input,optgroup,select,textarea{font:inherit;}optgroup{font-weight:700;}button{overflow:visible;}[type='button']::-moz-focus-inner,[type='reset']::-moz-focus-inner,[type='submit']::-moz-focus-inner,button::-moz-focus-inner{border-style:0;padding:0;}[type='button']::-moz-focus-inner,[type='reset']::-moz-focus-inner,[type='submit']::-moz-focus-inner,button:-moz-focusring{outline:1px dotted ButtonText;}[type='reset'],[type='submit'],button,html [type='button']{-webkit-appearance:button;}button,select{text-transform:none;}button,input,select,textarea{background-color:transparent;border-style:none;color:inherit;}select{-moz-appearance:none;-webkit-appearance:none;}select::-ms-expand{display:none;}select::-ms-value{color:currentColor;}legend{border:0;color:inherit;display:table;max-width:100%;white-space:normal;}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit;}[type='search']{-webkit-appearance:textfield;outline-offset:-2px;}img{border-style:none;}progress{vertical-align:baseline;}svg:not(:root){overflow:hidden;}audio,canvas,progress,video{display:inline-block;}@media screen{[hidden~='screen']{display:inherit;}[hidden~='screen']:not(:active):not(:focus):not(:target){position:absolute !important;clip:rect(0 0 0 0) !important;}}[aria-busy='true']{cursor:progress;}[aria-controls]{cursor:pointer;}[aria-disabled='true']{cursor:default;}::-moz-selection{background-color:#b3d4fc;color:#000;text-shadow:none;}::selection{background-color:#b3d4fc;color:#000;text-shadow:none;}ul{list-style:none;}a{text-decoration:none;}fieldset{border:0;}:-moz-ui-invalid,:-moz-ui-invalid:-moz-focusring{box-shadow:none;}"], '');

var headerHeight = 40;
var footerHeight = 20;
var sidebarOpenWidth = 270;
var sidebarClosedWidth = 51;

// Colour names must match names taken from https://developer.microsoft.com/en-us/fabric#/styles/colors
var darkGrey = '#232b2b';
var primaryBlue = '#0078d4';
var primaryLightBlue = '#71afe5';
var black = '#0e1111';
var white = '#eff6fc';
var defaultPalette = {
  themeDarker: black,
  themeDark: darkGrey,
  themeDarkAlt: '#353839',
  themePrimary: primaryBlue,
  themePicker: darkGrey,
  themeSecondary: '#414a4c',
  themeTertiary: black,
  themeLight: '#c7e0f4',
  themeLighter: '#d0d0d0',
  themeLighterAlt: '#c3c3c3',
  themeHeaderBgColor: darkGrey,
  themeFooterBgColor: darkGrey,
  themeSideBarBgColor: darkGrey,
  themeSideBodyBgColor: darkGrey,
  themeSideBarTextColor: white,
  themeButtonBgColor: primaryBlue,
  themeTitleBgColor: primaryLightBlue,
  themeButtonBgHover: primaryBlue,
  themeMenuHover: primaryBlue,
  themeMenuIconColor: primaryLightBlue,
  themeUserHeaderTextColor: '#eaeaea',
  themeUserHeaderBgColor: '#3c3c3c',
  themeMyAppSubTitle: '#707070',
  black: '#000000',
  neutralDark: '#212121',
  neutralPrimary: '#333333',
  neutralPrimaryAlt: '#3c3c3c',
  neutralSecondary: '#666666',
  neutralTertiary: '#a6a6a6',
  neutralTertiaryAlt: '#969696',
  neutralQuaternary: '#d0d0d0',
  neutralQuaternaryAlt: '#dadada',
  neutralLight: '#eaeaea',
  neutralLighter: '#f4f4f4',
  neutralLighterAlt: '#f8f8f8',
  white: '#ffffff',
  orange: '#ee8635',
  orangeLight: '#ea4300',
  orangeLighter: '#ff8c00',
  yellow: '#f7bd3a',
  yellowLight: '#fff100',
  green: '#acd336',
  greenDark: '#004b1c',
  greenLight: '#bad80a',
  red: '#e81123',
  redDark: '#a80000',
  brownLight: '#d5a972',
  brownDark: '#a14f2a',
  blueDark: '#002050',
  blueMid: '#00188f',
  blue: '#99cbfc',
  blueLight: '#00bcf2',
  tealDark: '#004b50',
  teal: '#008272',
  tealLight: '#00B294',
  magentaDark: '#5c005c',
  magenta: '#b4009e',
  magentaLight: '#e3008c',
  purpleDark: '#32145a',
  purple: '#5c2d91',
  purpleLight: '#b4a0ff'
};

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
var purple = '#9229E1';
var purpleDarkAlt = '#7421B3';
var themeColors = {
  themeDarker: '#381157',
  themeDark: '#561985',
  themeDarkAlt: purpleDarkAlt,
  themePrimary: purple,
  themePicker: purple,
  themeSecondary: '#A853E7',
  themeTertiary: '#BE7DED',
  themeLight: '#D4A7F3',
  themeLighter: '#DFBCF6',
  themeLighterAlt: '#EAD1F9',
  themeHeaderBgColor: purple,
  themeFooterBgColor: purple,
  themeSideBarTextColor: purple,
  themeSideBodyBgColor: purple,
  themeSideBarBgColor: '#ffffff',
  themeButtonBgColor: purple,
  themeTitleBgColor: '#eff6fc',
  themeButtonBgHover: purpleDarkAlt,
  themeMenuHover: '#eaeaea',
  themeMenuIconColor: purple,
  themeUserHeaderTextColor: '#000000',
  themeUserHeaderBgColor: '#DFBCF6',
  themeMyAppSubTitle: '#707070'
};
var halloweePalette = Object.assign({}, _objectSpread({}, defaultPalette, {}, themeColors));

function ownKeys$1(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$1(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$1(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$1(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
var primaryBlue$1 = '#0078d4';
var themeColors$1 = {
  themeDarker: '#004578',
  themeDark: '#005a9e',
  themeDarkAlt: '#106ebe',
  themePrimary: primaryBlue$1,
  themePicker: primaryBlue$1,
  themeSecondary: '#2b88d8',
  themeTertiary: '#71afe5',
  themeLight: '#c7e0f4',
  themeLighter: '#deecf9',
  themeLighterAlt: '#eff6fc',
  themeHeaderBgColor: primaryBlue$1,
  themeFooterBgColor: primaryBlue$1,
  themeSideBarTextColor: primaryBlue$1,
  themeSideBodyBgColor: primaryBlue$1,
  themeSideBarBgColor: '#ffffff',
  themeButtonBgColor: primaryBlue$1,
  themeTitleBgColor: '#eff6fc',
  themeButtonBgHover: '#106ebe',
  themeMenuHover: '#eaeaea',
  themeMenuIconColor: primaryBlue$1,
  themeUserHeaderTextColor: '#000000',
  themeUserHeaderBgColor: '#deecf9',
  themeMyAppSubTitle: '#707070'
};
var darkPalette = Object.assign({}, _objectSpread$1({}, defaultPalette, {}, themeColors$1));

function ownKeys$2(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$2(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$2(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$2(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var fixedAttributes = {
  quadrotechBlue: '#41b6e6',
  // Typography taken from https://developer.microsoft.com/en-us/fabric#/styles/typography
  fontSizeXS: '11px',
  fontSizeS: '12px',
  fontSizeM: '14px',
  fontSizeL: '17px',
  fontSizeXL: '21px',
  fontSizeXXL: '28px',
  fontSizeSU: '42px',
  fontWeightLight: '100',
  fontWeightSemiLight: '300',
  fontWeightRegular: '400',
  fontWeightSemiBold: '600',
  // Sizes
  chromeHeight: "".concat(headerHeight + footerHeight, "px"),
  headerHeight: "".concat(headerHeight, "px"),
  footerHeight: "".concat(footerHeight, "px"),
  widgetMinWidth: '350px',
  widgetMinHeight: '350px',
  alertDropDown: {
    maxWidth: '360px',
    minHeight: '150px',
    maxHeight: '60vh'
  },
  // Media Queries
  screenLarge: '1920px',
  // Shadow
  subtleShadow: 'rgba(0, 0, 0, 0.4) 0 0 6px 1px'
};
var defaultTheme = _objectSpread$2({}, defaultPalette, {}, fixedAttributes);
var themeData = [{
  name: 'Default',
  value: 'default',
  previewColor: defaultPalette.themePicker,
  palette: defaultPalette,
  data: defaultTheme
}, {
  name: 'Halloween',
  value: 'halloween',
  previewColor: halloweePalette.themePicker,
  palette: halloweePalette,
  data: _objectSpread$2({}, halloweePalette, {}, fixedAttributes)
}, {
  name: 'Fabric Blue',
  value: 'blue',
  previewColor: darkPalette.themePicker,
  palette: darkPalette,
  data: _objectSpread$2({}, darkPalette, {}, fixedAttributes)
}];

var theme = /*#__PURE__*/Object.freeze({
  __proto__: null,
  defaultTheme: defaultTheme,
  themeData: themeData,
  defaultPalette: defaultPalette
});

var print = css(["@page{margin:0;}"]);

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  padding: 5px;\n\n  [data-qt-custom-disabled='true'] {\n    opacity: 0.4;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}
var BarItemsWrapper = styled.div(_templateObject());

var BarItems = function BarItems(props) {
  var items = props.items,
      theme = props.theme,
      selectedKey = props.selectedKey;
  return React$1.createElement(BarItemsWrapper, null, React$1.createElement(Nav, {
    selectedKey: selectedKey,
    groups: [{
      links: items
    }],
    styles: {
      link: {
        selectors: {
          '.ms-Nav-compositeLink:hover &': {
            backgroundColor: theme.themeMenuHover
          },
          '.ms-Nav-compositeLink.is-selected &': {
            backgroundColor: theme.themeMenuHover
          },
          '.ms-Nav-compositeLink:hover & .ms-Button-icon': {
            color: theme.themeMenuIconColor
          },
          '.ms-Button-icon': {
            color: theme.themeMenuIconColor
          }
        }
      },
      linkText: {
        color: theme.themeSideBarTextColor
      },
      compositeLink: {
        backgroundColor: theme.themeSideBarBgColor
      },
      chevronIcon: {
        color: theme.themeMenuIconColor
      },
      chevronButton: {
        selectors: {
          '$compositeLink:hover &': {
            backgroundColor: theme.themeMenuHover
          }
        }
      }
    }
  }));
};

var SideBarNav = withTheme(BarItems);

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bold;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  width: 40px;\n  min-width: 40px;\n  background-color: ", ";\n  color: white;\n  font-size: 24px;\n  &:hover,\n  &:active {\n    ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  justify-content: ", ";\n  padding: 10px 10px 10px 0;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject$1() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n  min-width: ", ";\n  min-height: ", ";\n\n  @media print {\n    display: none;\n  }\n"]);

  _templateObject$1 = function _templateObject() {
    return data;
  };

  return data;
}
var sidebarOpenWidth$1 = sidebarOpenWidth,
    sidebarClosedWidth$1 = sidebarClosedWidth;
var StyledSideBar = styled.div(_templateObject$1(), function (props) {
  return props.theme.themeSideBodyBgColor;
}, function (props) {
  return props.isOpen ? "".concat(sidebarOpenWidth$1, "px") : "".concat(sidebarClosedWidth$1, "px");
}, function () {
  return "calc(\n        100vh\n        - ".concat(headerHeight, "px\n        - ").concat(footerHeight, "px\n        )");
});
var ButtonWrapper = styled.div(_templateObject2(), function (props) {
  return props.isOpen ? 'flex-end' : 'flex-start';
});
var ToggleButton = styled(DefaultButton)(_templateObject3(), function (props) {
  return props.theme.themeSideBodyBgColor;
}, function (_ref) {
  var theme = _ref.theme;
  return "\n      color: ".concat(theme.white, ";\n      background-color: ").concat(theme.themeButtonBgHover, ";\n    ");
});
var StyledIcon = styled(Icon)(_templateObject4());

var SideBar =
/*#__PURE__*/
function (_Component) {
  _inherits(SideBar, _Component);

  function SideBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, SideBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SideBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      isOpen: true
    });

    _defineProperty(_assertThisInitialized(_this), "onClick", function () {
      _this.setState(function (state) {
        return {
          isOpen: !state.isOpen
        };
      });
    });

    return _this;
  }

  _createClass(SideBar, [{
    key: "render",
    value: function render() {
      var config = this.props.config;
      var isOpen = this.state.isOpen;
      var iconName = isOpen ? 'ChevronLeft' : 'ChevronRight';
      return React$1.createElement(StyledSideBar, {
        isOpen: isOpen
      }, React$1.createElement(ButtonWrapper, {
        isOpen: isOpen
      }, React$1.createElement(ToggleButton, {
        onClick: this.onClick
      }, React$1.createElement(StyledIcon, {
        iconName: iconName
      }))), isOpen && React$1.createElement(SideBarNav, {
        items: config
      }));
    }
  }]);

  return SideBar;
}(Component);

var SideBarView =
/*#__PURE__*/
function (_React$Component) {
  _inherits(SideBarView, _React$Component);

  function SideBarView(props) {
    var _this;

    _classCallCheck(this, SideBarView);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(SideBarView).call(this, props));
    var config = props.config;

    if (!config) {
      throw new Error('Error creating Sidebar component! No config prop found.');
    }

    return _this;
  }

  _createClass(SideBarView, [{
    key: "render",
    value: function render() {
      return React$1.createElement(SideBar, {
        config: this.props.config
      });
    }
  }]);

  return SideBarView;
}(React$1.Component);

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\n  margin: 20px;\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  font-size: 20px;\n  color: ", ";\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  font-size: 50px;\n  transform: rotate(0.05turn) translateX(17px) translateY(-14.3px);\n  color: ", ";\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4$1() {
  var data = _taggedTemplateLiteral(["\n  transform: rotate(-0.55turn) translateX(-20px);\n  font-size: 30px;\n  color: ", ";\n"]);

  _templateObject4$1 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3$1() {
  var data = _taggedTemplateLiteral(["\n  display: block;\n  transform: translateX(-10px);\n"]);

  _templateObject3$1 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2$1() {
  var data = _taggedTemplateLiteral(["\n  text-align: center;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  padding: 100px;\n  border: 1px dashed ", ";\n  background-color: ", ";\n\n  h2 {\n    font-size: 40px;\n  }\n"]);

  _templateObject2$1 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject$2() {
  var data = _taggedTemplateLiteral(["\n  min-height: 100vh;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n"]);

  _templateObject$2 = function _templateObject() {
    return data;
  };

  return data;
}

var Styled404 = styled.div(_templateObject$2());
var Styled404Content = styled.div(_templateObject2$1(), function (_ref) {
  var theme = _ref.theme;
  return theme.themePrimary;
}, function (_ref2) {
  var theme = _ref2.theme;
  return theme.white;
});
var StyledIcongram = styled.div(_templateObject3$1());
var StyledWind = styled(Icon)(_templateObject4$1(), function (_ref3) {
  var theme = _ref3.theme;
  return theme.themePrimary;
});
var StyledPerson = styled(Icon)(_templateObject5(), function (_ref4) {
  var theme = _ref4.theme;
  return theme.themePrimary;
});
var StyledTripHazzard = styled(Icon)(_templateObject6(), function (_ref5) {
  var theme = _ref5.theme;
  return theme.themePrimary;
});
var StyledMessage = styled.p(_templateObject7());

var NotFound = function NotFound(_ref6) {
  var message = _ref6.message,
      t = _ref6.t,
      helpEmail = _ref6.supportEmail;
  // helper();
  var showEmailMessage = !!helpEmail;
  return React$1.createElement(Styled404, {
    className: "four-oh-four"
  }, React$1.createElement(Styled404Content, null, React$1.createElement(StyledIcongram, null, React$1.createElement(StyledWind, {
    iconName: "BlowingSnow"
  }), React$1.createElement(StyledPerson, {
    iconName: "Running"
  }), React$1.createElement(StyledTripHazzard, {
    iconName: "Cat"
  })), React$1.createElement("h2", null, "Whoops!"), React$1.createElement("div", null, React$1.createElement(StyledMessage, null, t(message)), showEmailMessage && React$1.createElement(StyledMessage, null, t('If this is not something you expected, please report this issue to {{ emailAddress }}', {
    emailAddress: helpEmail
  })))));
};

NotFound.defaultProps = {
  message: 'That page was not found'
};
var index = withNamespaces()(NotFound);

var UIThemeProvider = function UIThemeProvider(_ref) {
  var _ref$theme = _ref.theme,
      theme = _ref$theme === void 0 ? defaultTheme : _ref$theme,
      _ref$palette = _ref.palette,
      palette = _ref$palette === void 0 ? defaultPalette : _ref$palette,
      children = _ref.children;
  initializeIcons();
  loadTheme({
    palette: palette
  }); // Ensure MS Fabric theming matches app theming

  return React.createElement(ThemeProvider, {
    theme: theme
  }, children);
};

var QuadrotechLogo = (function () {
  return React$1.createElement("svg", {
    version: "1.1",
    id: "Layer_1",
    xmlns: "http://www.w3.org/2000/svg",
    x: "0px",
    y: "0px",
    viewBox: "0 0 2412.1 335.2",
    style: {
      enableBackground: 'new 0 0 2412.1 335.2'
    },
    xmlSpace: "preserve"
  }, React$1.createElement("g", null, React$1.createElement("path", {
    d: "M1128.5,0c0,44.1-0.1,88.3-0.1,132.4c0,63.7,0,127.4,0,191c0,2,0,4,0,6.3c-14.2,0-28,0-42.2,0c0-6.2,0-12.2,0-19 c-1.4,0.7-2.2,1-2.9,1.5c-39.5,33-103.6,28.9-140.5-7.4c-24.1-23.7-35.7-52.6-35.2-86.2c0.8-52.9,35.3-93.6,75.3-106.2 c34.8-11,67.7-6.3,97.7,15.3c0.9,0.6,1.8,1.2,3.1,2.2c0.3-1.3,0.6-2.3,0.6-3.3c0-41.5,0-82.9,0-124.4c0-0.8-0.2-1.5-0.3-2.3 C1098.9,0,1113.7,0,1128.5,0z M1084.4,220.9c0-12.4,0.1-24.9-0.1-37.3c0-1.9-0.5-4.3-1.6-5.6c-20.9-23.9-46.9-34.5-78.1-26.9 c-52.6,12.8-71,81.2-33,120.1c30.5,31.2,81.3,28.9,109.6-4.9c2.4-2.8,3.5-5.6,3.4-9.4C1084.3,244.8,1084.4,232.8,1084.4,220.9z"
  }), React$1.createElement("path", {
    d: "M177.8,0c7.5,1.3,15.1,2.4,22.6,3.9c65.1,13.1,118,68.9,127.7,134.6c1.3,8.5,1.8,17.1,2.7,26.2c-16.1,0-31.3,0-46.5,0 c0.7-66.8-57.6-122-125.1-118.2C95.8,50.1,47,100.8,46.4,164.3c-0.6,62.2,48,117.8,118.3,119.8c0,15.1,0,30.3,0,45.8 c-12.7,0.4-25.1-0.9-37-4.2c-69.3-18.7-111-63.5-125.5-133.7c-1-4.7-1.5-9.5-2.2-14.3c0-8.5,0-17,0-25.5c1-5.7,1.9-11.4,2.9-17 C14,70.7,66.1,17,130.2,3.9c7.3-1.5,14.7-2.6,22.1-3.9C160.8,0,169.3,0,177.8,0z"
  }), React$1.createElement("path", {
    d: "M2246.6,0c0,41.1,0.1,82.3,0.1,123.4c0,2,0,4,0,7.1c2.5-1.8,4-2.8,5.4-3.9c53.5-39.4,127.3-15.9,150.9,40.2 c6.4,15.2,8.7,31.3,8.8,47.5c0.4,37.4,0.1,74.7,0.1,112.1c0,1.1-0.1,2.1-0.2,3.4c-14.7,0-29.1,0-44,0c0-2.1,0-4,0-5.8 c0-36.4,0.3-72.8-0.2-109.2c-0.1-9.9-1.5-20.3-4.7-29.6c-7.3-21.1-23.5-32.9-45.3-35.7c-28.1-3.6-50.6,7.9-68.9,28.5 c-1.4,1.6-1.9,4.6-1.9,6.9c-0.1,46.2-0.1,92.4-0.1,138.5c0,2,0,4,0,6.3c-14.6,0-28.8,0-43.3,0c-0.1-1.3-0.4-2.6-0.4-4 c0-108.1,0-216.2,0-324.4c0-0.5,0.1-0.9,0.1-1.4C2217.6,0,2232.1,0,2246.6,0z"
  }), React$1.createElement("path", {
    d: "M863.5,329.9c-14.9,0-29.2,0-43.9,0c0-5.4,0-10.7,0-17c-2.2,1.4-3.5,2.3-4.9,3.1c-43,28.4-103.9,24.3-140.8-9.7 c-26-24.1-37.4-54.3-36.2-89.3c1.5-41.2,18.9-73.8,55-94.8c52.4-30.4,128.7-15.5,158.9,46c7.9,16.1,11.8,33.2,11.9,51 c0.1,35,0,70,0,105C863.5,325.9,863.5,327.6,863.5,329.9z M680.7,220.9c1.4,7.7,1.9,15.7,4.2,23.1c8.3,26.2,25.8,43.2,53.1,47.9 c26.3,4.6,49-3.2,65.9-24.6c13.8-17.4,17.5-37.5,14.5-59.2c-5.8-41.1-44.9-67.5-85.2-57.3C702.1,158.7,681.7,186.4,680.7,220.9z"
  }), React$1.createElement("path", {
    d: "M1927.7,229.9c-57.2,0-112.9,0-168.6,0c-1.8,18.6,12.8,43.7,32.5,56.1c23.7,14.9,70.4,17.4,100.5-19.2 c9.4,9,18.9,17.9,28.5,27.1c-9.6,12.4-21.2,21.8-35,28c-43,19.2-85.1,18.8-124.6-8.7c-25.6-17.8-39.7-43.4-44-74.1 c-4-28.9,0.1-56.6,15.3-81.8c17.7-29.3,43.8-45.7,77.9-49.4c57.5-6.3,104.7,32,114.6,88.8C1926.7,207.3,1926.8,218.4,1927.7,229.9z M1762.5,192.8c39.4,0,78.8,0,118.2,0c-3.4-21.2-25.9-46.4-55.6-47.2C1789.9,144.5,1766,172,1762.5,192.8z"
  }), React$1.createElement("path", {
    d: "M1423.8,334.7c-65.9,0.2-113.8-51-112.9-116c0.9-63.8,49.2-112,114.4-111.6c64.7,0.4,112.8,50,111.6,116.2 C1535.9,287.1,1488,334.9,1423.8,334.7z M1354.8,220.9c0.5,5,0.7,10.1,1.7,15c5.2,25.6,19.2,44.5,44.3,53.2 c24.9,8.6,48.5,4.9,68.4-12.8c20.4-18.1,26.5-41.7,22.8-68.1c-6.1-43.9-50.3-70.4-92-55.2C1372.5,162.9,1355.2,189.2,1354.8,220.9z "
  }), React$1.createElement("path", {
    d: "M432.9,112.2c0,1.8,0,3.7,0,5.5c0,38.1-0.1,76.3,0.1,114.4c0,6.6,0.7,13.2,1.9,19.7c5,27.3,31.9,43.5,57.9,41.2 c22.2-2,39.9-12.8,54.4-29c1.5-1.6,2.1-4.5,2.1-6.8c0.1-46.2,0.1-92.4,0.1-138.5c0-2,0-4.1,0-6.4c14.6,0,28.8,0,43.4,0 c0,72.5,0,144.9,0,217.6c-13.9,0-27.7,0-42.1,0c0-6.1,0-12.3,0-18.3c-7.8,4.7-14.9,9.6-22.5,13.4c-42,20.9-94.9,7.7-122.3-30.4 c-10.2-14.2-15.5-30.5-15.9-47.7c-0.9-44.4-0.8-88.8-1.1-133.3c0-0.3,0.1-0.6,0.2-1.3C403.5,112.2,417.8,112.2,432.9,112.2z"
  }), React$1.createElement("path", {
    d: "M2136.3,259.4c11.2,8.1,22.1,16.1,33.4,24.3c-5.2,9.4-12.2,17-20.3,23.5c-26.5,21.4-56.8,30.4-90.7,26.8 c-38.8-4.2-69.3-22.3-88.6-56.7c-29-51.7-15-128.6,48.2-158.8c48.6-23.2,107.2-11,141.6,32.1c0.9,1.1,1.6,2.3,2.8,3.9 c-10.7,9.1-21.4,18-31.6,26.7c-6.3-6-11.8-12.4-18.4-17.4c-21.7-16.5-45.7-20-70.7-9.3c-25.2,10.7-38.7,31-41.5,58 c-2.7,25.6,4.4,48,24.7,64.6c29.1,23.8,72.7,21.1,99.6-5.3C2128.7,267.8,2132.3,263.6,2136.3,259.4z"
  }), React$1.createElement("path", {
    d: "M1686.8,291.2c2.7,12,5.5,24.2,8,36.5c0.2,0.9-1.8,3.2-3,3.4c-12.2,1.4-24.4,3.5-36.5,3.4c-16-0.1-31-5-44.2-14.5 c-17.4-12.5-26.1-29.9-28.3-50.7c-0.8-7.7-1-15.4-1-23.1c-0.1-62.7,0-125.5,0-188.2c0-2,0-4,0-6.4c14.7,0,28.9,0,43.9,0 c0,19.8,0,39.7,0,60.1c23.6,0,46.5,0,69.8,0c0,13.3,0,26.1,0,39.4c-23.1,0-46,0-69.1,0c-0.2,1.1-0.5,1.9-0.5,2.6 c0,35.2,0.3,70.3,0.1,105.5c-0.1,24.5,14.8,32.9,35.3,33.8C1669.4,293.4,1677.7,291.9,1686.8,291.2z"
  }), React$1.createElement("path", {
    d: "M1292.9,150.1c-11.2-0.3-21.5-1.3-31.8-0.6c-15.3,1.1-25.5,9.5-28.2,22.3c-0.9,4.4-1.3,9.1-1.3,13.6 c-0.1,46.2-0.1,92.3-0.1,138.5c0,1.9,0,3.7,0,5.9c-14.6,0-28.9,0-43.8,0c-0.1-1.6-0.3-3.2-0.3-4.9c0-46.2-0.2-92.3,0.1-138.5 c0.1-19.3,3.8-37.9,17-53.2c12.5-14.5,28.6-22.3,47.3-25.1c15.6-2.3,31.1-1,46.4,2.7c0.9,0.2,1.7,0.6,3,1.1 C1298.4,125,1295.5,137.9,1292.9,150.1z"
  }), React$1.createElement("path", {
    d: "M283.8,188.6c15.7,0,30.7,0,46.1,0c0,47.1,0,94,0,141.1c-46.9,0-93.7,0-140.9,0c0-15.1,0-30.3,0-45.8c31.5,0,62.8,0,94.8,0 C283.8,252,283.8,220.4,283.8,188.6z"
  })));
});

var LoadingSpinner = function LoadingSpinner(_ref) {
  var label = _ref.label,
      t = _ref.t;
  return React$1.createElement(Spinner, {
    size: SpinnerSize.large,
    label: t(label)
  });
};

LoadingSpinner.defaultProps = {
  label: ''
};
var LoadingSpinner$1 = withNamespaces()(LoadingSpinner);

function _templateObject5$1() {
  var data = _taggedTemplateLiteral(["\n  width: 100px;\n  align-self: flex-end;\n"]);

  _templateObject5$1 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4$2() {
  var data = _taggedTemplateLiteral(["\n  text-align: center;\n  margin: 5px 0;\n  font-size: 18px;\n"]);

  _templateObject4$2 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3$2() {
  var data = _taggedTemplateLiteral(["\n  padding-bottom: 20px;\n"]);

  _templateObject3$2 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2$2() {
  var data = _taggedTemplateLiteral(["\n  padding: 20px;\n\n  & > svg {\n    display: block;\n    fill: ", ";\n  }\n"]);

  _templateObject2$2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject$3() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n  width: 400px;\n  padding: 20px;\n"]);

  _templateObject$3 = function _templateObject() {
    return data;
  };

  return data;
}
var StyledLoginModal = styled.div(_templateObject$3());
var LogoWrapper = styled.div(_templateObject2$2(), function (_ref) {
  var theme = _ref.theme;
  return theme.quadrotechBlue;
});
var TextWrapper = styled.div(_templateObject3$2());
var Paragraph = styled.p(_templateObject4$2());
var SignInButton = styled(DefaultButton)(_templateObject5$1());

var LoginModal =
/*#__PURE__*/
function (_Component) {
  _inherits(LoginModal, _Component);

  function LoginModal() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LoginModal);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LoginModal)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "handleButtonClick", function (event) {
      event.preventDefault();
      var launchSigninRedirect = _this.props.launchSigninRedirect;
      launchSigninRedirect();
    });

    return _this;
  }

  _createClass(LoginModal, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          isOpen = _this$props.isOpen,
          checkingAuthSession = _this$props.checkingAuthSession,
          authStatus = _this$props.authStatus,
          t = _this$props.t;
      return React$1.createElement(Modal, {
        isOpen: isOpen,
        isBlocking: true
      }, React$1.createElement(StyledLoginModal, null, React$1.createElement(LogoWrapper, null, React$1.createElement(QuadrotechLogo, null)), authStatus && React$1.createElement(MessageBar, {
        styles: {
          root: {
            marginTop: 10,
            marginBottom: 10
          }
        }
      }, authStatus), !checkingAuthSession ? React$1.createElement(Fragment, null, React$1.createElement(TextWrapper, null, React$1.createElement(Paragraph, null, t("You're not signed in.")), React$1.createElement(Paragraph, null, t('Click below to sign in to your account.'))), React$1.createElement(SignInButton, {
        "data-automation-id": "login-button",
        text: t('Sign In'),
        primary: true,
        onClick: this.handleButtonClick
      })) : React$1.createElement(LoadingSpinner$1, {
        label: t('Checking authentication session')
      })));
    }
  }]);

  return LoginModal;
}(Component);

_defineProperty(LoginModal, "defaultProps", {
  authStatus: null
});

var index$1 = withNamespaces()(LoginModal);

function _templateObject$4() {
  var data = _taggedTemplateLiteral(["\n  margin-bottom: 10px;\n  font-weight: normal;\n  background-color: #ffffff;\n  padding: 10px 20px;\n  position: relative;\n  z-index: 2;\n"]);

  _templateObject$4 = function _templateObject() {
    return data;
  };

  return data;
}
var Header = styled.header(_templateObject$4());

function _templateObject$5() {
  var data = _taggedTemplateLiteral(["\n  font-size: 2rem;\n  color: #969696;\n  font-weight: normal;\n  padding: 5px 0;\n  margin: 0;\n"]);

  _templateObject$5 = function _templateObject() {
    return data;
  };

  return data;
}
var Title = styled.h1(_templateObject$5());

function _templateObject$6() {
  var data = _taggedTemplateLiteral(["\n"]);

  _templateObject$6 = function _templateObject() {
    return data;
  };

  return data;
}
var Body = styled.div(_templateObject$6());



var index$2 = /*#__PURE__*/Object.freeze({
  __proto__: null,
  Header: Header,
  Title: Title,
  Body: Body
});

export { index$2 as Content, LoadingSpinner$1 as LoadingSpinner, index$1 as LoginModal, QuadrotechLogo as Logo, index as NotFound, SideBarView as SideBar, UIThemeProvider as ThemeProvider, theme };
//# sourceMappingURL=index.js.map
