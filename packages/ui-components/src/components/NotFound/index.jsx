// @flow
import React from 'react';
import styled from 'styled-components';
import { withNamespaces } from 'react-i18next';
import { Icon } from 'office-ui-fabric-react/lib/Icon';
// import helper from '../../hooks/helper';

type Props = {
  message?: string,
  t: TranslatorFunction,
  supportEmail: string,
};

const Styled404 = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Styled404Content = styled.div`
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 100px;
  border: 1px dashed ${({ theme }) => theme.themePrimary};
  background-color: ${({ theme }) => theme.white};

  h2 {
    font-size: 40px;
  }
`;

const StyledIcongram = styled.div`
  display: block;
  transform: translateX(-10px);
`;

const StyledWind = styled(Icon)`
  transform: rotate(-0.55turn) translateX(-20px);
  font-size: 30px;
  color: ${({ theme }) => theme.themePrimary};
`;

const StyledPerson = styled(Icon)`
  font-size: 50px;
  transform: rotate(0.05turn) translateX(17px) translateY(-14.3px);
  color: ${({ theme }) => theme.themePrimary};
`;
const StyledTripHazzard = styled(Icon)`
  font-size: 20px;
  color: ${({ theme }) => theme.themePrimary};
`;

const StyledMessage = styled.p`
  margin: 20px;
`;

const NotFound = ({ message, t, supportEmail: helpEmail }: Props) => {
  // helper();

  const showEmailMessage = !!helpEmail;

  return (
    <Styled404 className="four-oh-four">
      <Styled404Content>
        <StyledIcongram>
          <StyledWind iconName="BlowingSnow" />
          <StyledPerson iconName="Running" />
          <StyledTripHazzard iconName="Cat" />
        </StyledIcongram>
        <h2>Whoops!</h2>
        <div>
          <StyledMessage>{t(message)}</StyledMessage>

          {showEmailMessage && (
            <StyledMessage>
              {t(
                'If this is not something you expected, please report this issue to {{ emailAddress }}',
                {
                  emailAddress: helpEmail,
                },
              )}
            </StyledMessage>
          )}
        </div>
      </Styled404Content>
    </Styled404>
  );
};

NotFound.defaultProps = {
  message: 'That page was not found',
};

export default withNamespaces()(NotFound);
