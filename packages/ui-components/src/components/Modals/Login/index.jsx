// @flow
import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Modal } from 'office-ui-fabric-react/lib/Modal';
import { MessageBar } from 'office-ui-fabric-react/lib/MessageBar';

import { withNamespaces } from 'react-i18next';

import QuadrotechLogo from '../../Logo';
import LoadingSpinner from '../../LoadingSpinner';

type Props = {
  isOpen: boolean,
  launchSigninRedirect: Function,
  checkingAuthSession: boolean,
  authStatus?: string,
  t: TranslatorFunction,
};

const StyledLoginModal = styled.div`
  display: flex;
  flex-direction: column;
  width: 400px;
  padding: 20px;
`;

const LogoWrapper = styled.div`
  padding: 20px;

  & > svg {
    display: block;
    fill: ${({ theme }) => theme.quadrotechBlue};
  }
`;

const TextWrapper = styled.div`
  padding-bottom: 20px;
`;

const Paragraph = styled.p`
  text-align: center;
  margin: 5px 0;
  font-size: 18px;
`;

const SignInButton = styled(DefaultButton)`
  width: 100px;
  align-self: flex-end;
`;

class LoginModal extends Component<Props> {
  static defaultProps = {
    authStatus: null,
  };

  handleButtonClick = (event: SyntheticEvent<HTMLElement>) => {
    event.preventDefault();
    const { launchSigninRedirect } = this.props;
    launchSigninRedirect();
  };

  render() {
    const { isOpen, checkingAuthSession, authStatus, t } = this.props;

    return (
      <Modal isOpen={isOpen} isBlocking>
        <StyledLoginModal>
          <LogoWrapper>
            <QuadrotechLogo />
          </LogoWrapper>

          {authStatus && (
            <MessageBar styles={{ root: { marginTop: 10, marginBottom: 10 } }}>
              {authStatus}
            </MessageBar>
          )}

          {!checkingAuthSession ? (
            <Fragment>
              <TextWrapper>
                <Paragraph>{t("You're not signed in.")}</Paragraph>
                <Paragraph>{t('Click below to sign in to your account.')}</Paragraph>
              </TextWrapper>

              <SignInButton
                data-automation-id="login-button"
                text={t('Sign In')}
                primary
                onClick={this.handleButtonClick}
              />
            </Fragment>
          ) : (
            <LoadingSpinner label={t('Checking authentication session')} />
          )}
        </StyledLoginModal>
      </Modal>
    );
  }
}

export default withNamespaces()(LoginModal);
