// @flow
import React from 'react';
import styled, { withTheme } from 'styled-components';
import { Nav } from 'office-ui-fabric-react/lib/Nav';

import type { RawSideBarItemType } from './types';

type Props = {
  items: Array<RawSideBarItemType>,
  theme: any,
  selectedKey: string,
};

const BarItemsWrapper = styled.div`
  padding: 5px;

  [data-qt-custom-disabled='true'] {
    opacity: 0.4;
  }
`;

const BarItems = (props: Props) => {
  const { items, theme, selectedKey } = props;

  return (
    <BarItemsWrapper>
      <Nav
        selectedKey={selectedKey}
        groups={[{ links: items }]}
        styles={{
          link: {
            selectors: {
              '.ms-Nav-compositeLink:hover &': {
                backgroundColor: theme.themeMenuHover,
              },
              '.ms-Nav-compositeLink.is-selected &': {
                backgroundColor: theme.themeMenuHover,
              },
              '.ms-Nav-compositeLink:hover & .ms-Button-icon': {
                color: theme.themeMenuIconColor,
              },
              '.ms-Button-icon': {
                color: theme.themeMenuIconColor,
              },
            },
          },

          linkText: {
            color: theme.themeSideBarTextColor,
          },
          compositeLink: {
            backgroundColor: theme.themeSideBarBgColor,
          },
          chevronIcon: {
            color: theme.themeMenuIconColor,
          },
          chevronButton: {
            selectors: {
              '$compositeLink:hover &': {
                backgroundColor: theme.themeMenuHover,
              },
            },
          },
        }}
      />
    </BarItemsWrapper>
  );
};

export default withTheme(BarItems);
