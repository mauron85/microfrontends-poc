import React from 'react';
import { shallow } from 'enzyme';

import Sidebar, { ToggleButton } from './SideBar';

describe('Sidebar component', () => {
  it('renders a toggle button', () => {
    const sidebar = shallow(<Sidebar items={[]} />);
    const toggleButton = sidebar.find(ToggleButton);

    expect(toggleButton).toHaveLength(1);
  });

  it('is open by default', () => {
    const sidebar = shallow(<Sidebar items={[]} />);

    expect(sidebar.state('isOpen')).toBe(true);
  });

  it('clicking the toggle button toggles open/close', () => {
    const sidebar = shallow(<Sidebar items={[]} />);
    const toggleButton = sidebar.find(ToggleButton);

    expect(sidebar.state('isOpen')).toBe(true);
    toggleButton.simulate('click');
    expect(sidebar.state('isOpen')).toBe(false);
    toggleButton.simulate('click');
    expect(sidebar.state('isOpen')).toBe(true);
  });
});
