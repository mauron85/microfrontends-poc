// @flow
import React, { Component } from 'react';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import styled from 'styled-components';
import type { RawSideBarItemType } from './types';
import { config as stylesConfig } from '../../styles';

import SideBarNav from './BarItems';

const { sidebarOpenWidth, sidebarClosedWidth } = stylesConfig;

type Props = {
  config: Array<RawSideBarItemType>,
};

type State = {
  isOpen: boolean,
};

const StyledSideBar = styled.div`
  background-color: ${props => props.theme.themeSideBodyBgColor};
  min-width: ${props => (props.isOpen ? `${sidebarOpenWidth}px` : `${sidebarClosedWidth}px`)};
  min-height: ${() => `calc(
        100vh
        - ${stylesConfig.headerHeight}px
        - ${stylesConfig.footerHeight}px
        )`};

  @media print {
    display: none;
  }
`;

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${props => (props.isOpen ? 'flex-end' : 'flex-start')};
  padding: 10px 10px 10px 0;
`;

export const ToggleButton = styled(DefaultButton)`
  width: 40px;
  min-width: 40px;
  background-color: ${props => props.theme.themeSideBodyBgColor};
  color: white;
  font-size: 24px;
  &:hover,
  &:active {
    ${({ theme }) => `
      color: ${theme.white};
      background-color: ${theme.themeButtonBgHover};
    `};
`;

const StyledIcon = styled(Icon)`
  font-weight: bold;
`;

export default class SideBar extends Component<Props, State> {
  state = {
    isOpen: true,
  };

  onClick = () => {
    this.setState(state => ({ isOpen: !state.isOpen }));
  };

  render() {
    const { config } = this.props;
    const { isOpen } = this.state;
    const iconName = isOpen ? 'ChevronLeft' : 'ChevronRight';

    return (
      <StyledSideBar isOpen={isOpen}>
        <ButtonWrapper isOpen={isOpen}>
          <ToggleButton onClick={this.onClick}>
            <StyledIcon iconName={iconName} />
          </ToggleButton>
        </ButtonWrapper>

        {isOpen && <SideBarNav items={config} />}
      </StyledSideBar>
    );
  }
}
