// @flow
import React from 'react';

import SideBar from './SideBar';

import type { RawSideBarItemType } from './types';

type Props = {
  config: Array<RawSideBarItemType>,
  nocMode: boolean,
  appIsReady: boolean,
  printMode: boolean,
};

class SideBarView extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    const { config } = props;

    if (!config) {
      throw new Error('Error creating Sidebar component! No config prop found.');
    }
  }

  render() {
    return <SideBar config={this.props.config} />;
  }
}

export default SideBarView;
