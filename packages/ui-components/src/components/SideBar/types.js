// @flow

// Your sidebar items should follow this structure:
export type RawSideBarItemType = {
  name: string,
  roles: Array<string>,
  relativeLink?: string,
  url?: string,
  onClick?: any => any,
  links?: Array<RawSideBarItemType>,
  icon?: string,
  subscriptions: Array<SubscriptionItemType>,
};
// ...and are converted into this structure
export type ParsedSideBarItem = {
  name: string,
  onClick?: any => any,
  url?: string,
  links?: Array<ParsedSideBarItem>,
  iconProps?: {
    iconName: string,
  },
};
