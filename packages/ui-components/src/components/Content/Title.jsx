import styled from 'styled-components';

const Title = styled.h1`
  font-size: 2rem;
  color: #969696;
  font-weight: normal;
  padding: 5px 0;
  margin: 0;
`;

export default Title;
