import styled from 'styled-components';

const Header = styled.header`
  margin-bottom: 10px;
  font-weight: normal;
  background-color: #ffffff;
  padding: 10px 20px;
  position: relative;
  z-index: 2;
`;

export default Header;
