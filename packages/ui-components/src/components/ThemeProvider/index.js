import { ThemeProvider } from 'styled-components';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';
import { loadTheme } from 'office-ui-fabric-react/lib/Styling';
import { defaultPalette, defaultTheme  } from '../../styles/theme';

const UIThemeProvider = ({ theme = defaultTheme, palette = defaultPalette, children }) => {
  initializeIcons();
  loadTheme({ palette }); // Ensure MS Fabric theming matches app theming
  return (
    <ThemeProvider theme={theme}>
      {children}
    </ThemeProvider>
  );
};

export default UIThemeProvider;
