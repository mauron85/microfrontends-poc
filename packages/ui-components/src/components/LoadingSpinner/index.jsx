// @flow
import React from 'react';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';
import { withNamespaces } from 'react-i18next';

type Props = {
  label?: string,
  t: TranslatorFunction,
};

const LoadingSpinner = ({ label, t }: Props) => (
  <Spinner size={SpinnerSize.large} label={t(label)} />
);

LoadingSpinner.defaultProps = {
  label: '',
};

export default withNamespaces()(LoadingSpinner);
