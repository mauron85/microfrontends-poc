const graphColours = [
  'red',
  'yellow',
  'orange',
  'green',
  'blueLight',
  'greenDark',
  'blueMid',
  'orangeLighter',
  'tealLight',
  'tealDark',
  'magentaLight',
  'purpleDark',
  'yellowLight',
  'magentaDark',
  'teal',
  'magenta',
  'greenLight',
  'brownLight',
  'redDark',
  'purpleLight',
  'brownDark',
  'blue',
  'blueDark',
  'orangeLight',
  'purple',
];

export const colorRange = (number = 15, priorityColors = []) => {
  if (priorityColors.length >= number) {
    return [...priorityColors];
  }

  // Omit colours that were already supplied
  const useColors = graphColours.filter(color => !priorityColors.includes(color));
  const generateNumber = number - priorityColors.length;

  const generatedRange = Array(generateNumber)
    .fill()
    .map((value, index) => {
      const colourIndex = index <= useColors.length - 1 ? index : index % useColors.length;
      return useColors[colourIndex];
    });

  return [...priorityColors, ...generatedRange];
};

export default colorRange;
