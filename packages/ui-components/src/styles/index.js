import { css } from 'styled-components';

export * as colours from './colours';
export { default as reset } from './reset';
export * as config from './config';
export * as theme from './theme';

export const print = css`
  @page {
    margin: 0;
  }
`;
