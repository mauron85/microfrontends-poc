// Colour names must match names taken from https://developer.microsoft.com/en-us/fabric#/styles/colors
const darkGrey = '#232b2b';
const primaryBlue = '#0078d4';
const primaryLightBlue = '#71afe5';
const black = '#0e1111';
const white = '#eff6fc';

export default {
  themeDarker: black,
  themeDark: darkGrey,
  themeDarkAlt: '#353839',
  themePrimary: primaryBlue,
  themePicker: darkGrey,
  themeSecondary: '#414a4c',
  themeTertiary: black,
  themeLight: '#c7e0f4',
  themeLighter: '#d0d0d0',
  themeLighterAlt: '#c3c3c3',

  themeHeaderBgColor: darkGrey,
  themeFooterBgColor: darkGrey,
  themeSideBarBgColor: darkGrey,
  themeSideBodyBgColor: darkGrey,
  themeSideBarTextColor: white,
  themeButtonBgColor: primaryBlue,
  themeTitleBgColor: primaryLightBlue,
  themeButtonBgHover: primaryBlue,
  themeMenuHover: primaryBlue,
  themeMenuIconColor: primaryLightBlue,
  themeUserHeaderTextColor: '#eaeaea',
  themeUserHeaderBgColor: '#3c3c3c',
  themeMyAppSubTitle: '#707070',

  black: '#000000',
  neutralDark: '#212121',
  neutralPrimary: '#333333',
  neutralPrimaryAlt: '#3c3c3c',
  neutralSecondary: '#666666',
  neutralTertiary: '#a6a6a6',
  neutralTertiaryAlt: '#969696',
  neutralQuaternary: '#d0d0d0',
  neutralQuaternaryAlt: '#dadada',
  neutralLight: '#eaeaea',
  neutralLighter: '#f4f4f4',
  neutralLighterAlt: '#f8f8f8',
  white: '#ffffff',

  orange: '#ee8635',
  orangeLight: '#ea4300',
  orangeLighter: '#ff8c00',

  yellow: '#f7bd3a',
  yellowLight: '#fff100',

  green: '#acd336',
  greenDark: '#004b1c',
  greenLight: '#bad80a',

  red: '#e81123',
  redDark: '#a80000',

  brownLight: '#d5a972',
  brownDark: '#a14f2a',

  blueDark: '#002050',
  blueMid: '#00188f',
  blue: '#99cbfc',
  blueLight: '#00bcf2',

  tealDark: '#004b50',
  teal: '#008272',
  tealLight: '#00B294',

  magentaDark: '#5c005c',
  magenta: '#b4009e',
  magentaLight: '#e3008c',

  purpleDark: '#32145a',
  purple: '#5c2d91',
  purpleLight: '#b4a0ff',
};
