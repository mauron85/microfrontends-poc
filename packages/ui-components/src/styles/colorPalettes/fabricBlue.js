// Colours taken from https://developer.microsoft.com/en-us/fabric#/styles/colors
import defaultPalette from './default';

const primaryBlue = '#0078d4';

const themeColors = {
  themeDarker: '#004578',
  themeDark: '#005a9e',
  themeDarkAlt: '#106ebe',
  themePrimary: primaryBlue,
  themePicker: primaryBlue,
  themeSecondary: '#2b88d8',
  themeTertiary: '#71afe5',
  themeLight: '#c7e0f4',
  themeLighter: '#deecf9',
  themeLighterAlt: '#eff6fc',

  themeHeaderBgColor: primaryBlue,
  themeFooterBgColor: primaryBlue,
  themeSideBarTextColor: primaryBlue,
  themeSideBodyBgColor: primaryBlue,
  themeSideBarBgColor: '#ffffff',
  themeButtonBgColor: primaryBlue,
  themeTitleBgColor: '#eff6fc',
  themeButtonBgHover: '#106ebe',
  themeMenuHover: '#eaeaea',
  themeMenuIconColor: primaryBlue,
  themeUserHeaderTextColor: '#000000',
  themeUserHeaderBgColor: '#deecf9',
  themeMyAppSubTitle: '#707070',
};

const darkPalette = Object.assign({}, { ...defaultPalette, ...themeColors });
export default darkPalette;
