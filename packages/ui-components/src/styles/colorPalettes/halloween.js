import defaultPalette from './default';

const purple = '#9229E1';
const purpleDarkAlt = '#7421B3';

const themeColors = {
  themeDarker: '#381157',
  themeDark: '#561985',
  themeDarkAlt: purpleDarkAlt,
  themePrimary: purple,
  themePicker: purple,
  themeSecondary: '#A853E7',
  themeTertiary: '#BE7DED',
  themeLight: '#D4A7F3',
  themeLighter: '#DFBCF6',
  themeLighterAlt: '#EAD1F9',

  themeHeaderBgColor: purple,
  themeFooterBgColor: purple,
  themeSideBarTextColor: purple,
  themeSideBodyBgColor: purple,
  themeSideBarBgColor: '#ffffff',
  themeButtonBgColor: purple,
  themeTitleBgColor: '#eff6fc',
  themeButtonBgHover: purpleDarkAlt,
  themeMenuHover: '#eaeaea',
  themeMenuIconColor: purple,
  themeUserHeaderTextColor: '#000000',
  themeUserHeaderBgColor: '#DFBCF6',
  themeMyAppSubTitle: '#707070',
};

const halloweePalette = Object.assign({}, { ...defaultPalette, ...themeColors });
export default halloweePalette;
