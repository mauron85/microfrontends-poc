import { headerHeight, footerHeight } from './config';
import defaultPalette from './colorPalettes/default';
import halloweenPalette from './colorPalettes/halloween';
import bluePalette from './colorPalettes/fabricBlue';

export { default as defaultPalette } from './colorPalettes/default';

// Themes used by Styled Components theme provider
const fixedAttributes = {
  quadrotechBlue: '#41b6e6',

  // Typography taken from https://developer.microsoft.com/en-us/fabric#/styles/typography
  fontSizeXS: '11px',
  fontSizeS: '12px',
  fontSizeM: '14px',
  fontSizeL: '17px',
  fontSizeXL: '21px',
  fontSizeXXL: '28px',
  fontSizeSU: '42px',

  fontWeightLight: '100',
  fontWeightSemiLight: '300',
  fontWeightRegular: '400',
  fontWeightSemiBold: '600',

  // Sizes
  chromeHeight: `${headerHeight + footerHeight}px`,
  headerHeight: `${headerHeight}px`,
  footerHeight: `${footerHeight}px`,
  widgetMinWidth: '350px',
  widgetMinHeight: '350px',

  alertDropDown: {
    maxWidth: '360px',
    minHeight: '150px',
    maxHeight: '60vh',
  },

  // Media Queries
  screenLarge: '1920px',

  // Shadow
  subtleShadow: 'rgba(0, 0, 0, 0.4) 0 0 6px 1px',
};

export const defaultTheme = {
  ...defaultPalette,
  ...fixedAttributes,
};

export const themeData = [
  {
    name: 'Default',
    value: 'default',
    previewColor: defaultPalette.themePicker,
    palette: defaultPalette,
    data: defaultTheme,
  },
  {
    name: 'Halloween',
    value: 'halloween',
    previewColor: halloweenPalette.themePicker,
    palette: halloweenPalette,
    data: {
      ...halloweenPalette,
      ...fixedAttributes,
    },
  },
  {
    name: 'Fabric Blue',
    value: 'blue',
    previewColor: bluePalette.themePicker,
    palette: bluePalette,
    data: {
      ...bluePalette,
      ...fixedAttributes,
    },
  },
];
