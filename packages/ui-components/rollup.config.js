import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import css from 'rollup-plugin-css-only';
import json from 'rollup-plugin-json';
import visualizer from 'rollup-plugin-visualizer';
import replace from 'rollup-plugin-replace';

import pkg from './package.json';

const { libBuild, libBuildESM } = require('./config/paths');

const external = id => !id.startsWith('\0') && !id.startsWith('.') && !id.startsWith('/');

const getPlugins = () => [
  resolve({ extensions: ['.js', '.jsx', '.json'], preferBuiltins: true }),
  replace({
    'process.env.UI_CORE_VERSION': JSON.stringify(pkg.version),
  }),
  babel({
    exclude: 'node_modules/**',
    runtimeHelpers: true,
  }),
  json(),
  css(),
];

export default [
  // ES Module Version of library
  {
    input: {
      index: 'src/index.js',
      // withAuth: 'src/lib/hoc/withAuth.jsx',
      // getRouteRoles: 'src/lib/utils/getRouteRoles.js',
      // NotFound: 'src/lib/components/NotFound/index.jsx',
      // Page: 'src/lib/components/Page/index.jsx',
      // PageHeader: 'src/lib/components/PageHeader/index.jsx',
      // MyApps: 'src/lib/components/MyApps/index.jsx',
      // withProductEndpoints: 'src/lib/hoc/withProductEndpoints.js',
      // ROLES: 'src/lib/config/roles.js',
      // QuadrotechAppFactory: 'src/lib/factories/QuadrotechApp.jsx',
    },
    external,
    output: {
      dir: libBuildESM,
      format: 'esm',
      chunkFileNames: 'common/[name]-[hash].js',
      sourcemap: true,
    },
    plugins: [...getPlugins(), visualizer({ filename: 'bundle-size-stats.html' })],
  },
  // UMD (CommonJS) version of library
  {
    input: 'src/index.js',
    external,
    output: {
      dir: libBuild,
      format: 'umd',
      name: 'index.js',
      sourcemap: true,
    },
    plugins: getPlugins(),
  },
];
