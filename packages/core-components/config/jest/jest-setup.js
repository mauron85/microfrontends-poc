import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { setIconOptions } from 'office-ui-fabric-react/lib/Styling';
import i18n from 'i18next';

configure({ adapter: new Adapter() });

jest.mock('decimal.js-light', () => {});

i18n.t = jest.fn().mockImplementation(str => str);

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate HoC receive the t function as a prop
  withNamespaces: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: text => text };
    return Component;
  },
}));

// Suppress icon warnings.
setIconOptions({
  disableWarnings: true,
});
