import { Route, useHistory } from 'react-router-dom';

const PrivateRoute = ({ children, ...rest }) => {
  const history = useHistory();

  // useEffect(() => {
    if (!user.isAuthenticated) {
      history.push('/signin');
      return null;
    }
  // }, [user]);

  return <Route {...rest} />;
};

export default PrivateRoute;
